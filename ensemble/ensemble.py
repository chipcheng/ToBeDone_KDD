import argparse
import logging
import yaml
import os
import requests
import warnings
from datetime import datetime
from collections import defaultdict

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression


logger = logging.getLogger(__name__)

ld_stations = [
    'BL0', 'CD1', 'CD9', 'GN0', 'GN3', 'GR4',
    'GR9', 'HV1', 'KF1', 'LW2', 'MY7', 'ST5', 'TH4'
]
bj_stations = [
    'aotizhongx_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq',
    'dingling_aq', 'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq',
    'fangshan_aq', 'fengtaihua_aq', 'guanyuan_aq', 'gucheng_aq',
    'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq',
    'miyunshuik_aq', 'nansanhuan_aq', 'nongzhangu_aq', 'pingchang_aq',
    'pinggu_aq', 'qianmen_aq', 'shunyi_aq', 'tiantan_aq', 'tongzhou_aq',
    'wanliu_aq', 'wanshouxig_aq', 'xizhimenbe_aq', 'yanqin_aq',
    'yizhuang_aq', 'yongdingme_aq', 'yongledian_aq', 'yufa_aq',
    'yungang_aq', 'zhiwuyuan_aq'
]


def upload_submission(path, desc):
    files = {'files': open(path, 'rb')}

    token = "a3159ee92a5771ae7ee4d723aa0f2b479a05771d92f2aed37902fb43588605b1"

    data = {
        "user_id": "kevinorjohn",
        "team_token": token,  # your team_token.
        "description": desc,  # no more than 40 chars.
        "filename": os.path.basename(path),
    }

    url = 'https://biendata.com/competition/kdd_2018_submit/'
    response = requests.post(url, files=files, data=data)

    logger.info(response.text)


def smape(actual, predicted):
    a = np.abs(np.array(actual) - np.array(predicted))
    b = np.array(actual) + np.array(predicted)

    score = 2 * np.mean(
        np.divide(a, b, out=np.zeros_like(a), where=(b != 0), casting='unsafe')
    )
    return score


def load_prediction(config, date, mode):
    # read files
    df_dict = {}
    for filename in os.listdir(config['{}_dir'.format(mode)]):
        if date in filename:
            logger.info("Loading {}...".format(filename))
            df = pd.read_csv(
                os.path.join(config['{}_dir'.format(mode)], filename),
                parse_dates=['submit_date']
            )
            tmp = df['test_id'].str.split('#', expand=True)
            tmp.columns = ['station_id', 'hour']
            df = pd.concat([df, tmp], axis=1)
            df['hour'] = pd.to_numeric(df['hour'])
            offset = pd.to_timedelta(df['hour'] + 24, unit='h')
            df['time'] = df['submit_date'].add(offset)
            # df.drop(['hour'], axis=1, inplace=True)
            filename = filename.replace("{}_".format(mode), "")
            columns = ['PM2.5', 'PM10', 'O3']

            # replace negative prediction with zero
            df[columns] = df[columns].mask(df[columns] < 0, 0.)

            df.set_index(
                ['station_id', 'time', 'submit_date', 'test_id', 'hour'],
                inplace=True
            )
            df_dict[filename] = df
    return df_dict


def load_files(config, date):
    ''' read prediction files '''
    valid_df_dict = load_prediction(config, date, 'valid')
    test_df_dict = load_prediction(config, date, 'test')

    # check integrity
    keys = set(valid_df_dict.keys()) & set(test_df_dict.keys())
    if len(valid_df_dict.keys()) != len(test_df_dict.keys()):
        warnings.warn("# of test files and # of valid files are not equal")
    if not keys:
        raise Exception("test files and valid files cannot be empty")
    # combine testing files and validation files
    data = [(valid_df_dict[key], test_df_dict[key]) for key in keys]
    valid_dfs, test_dfs = zip(*data)
    valid_df = pd.concat(valid_dfs, axis=1)
    test_df = pd.concat(test_dfs, axis=1)

    # replace london O3 with nan value
    mask = valid_df.index.get_level_values('station_id').isin(ld_stations)
    valid_df.loc[mask, 'O3'] = np.nan
    mask = test_df.index.get_level_values('station_id').isin(ld_stations)
    test_df.loc[mask, 'O3'] = np.nan

    ''' read label files '''
    # read bj station label
    filename = "bj_aq.csv"
    bj_df = pd.read_csv(
        os.path.join(config['label_dir'], filename),
        parse_dates=['utc_time']
    )
    bj_df.rename(
        columns={'stationId': 'station_id', 'utc_time': 'time'},
        inplace=True
    )
    bj_df['station_id'] = bj_df['station_id'].apply(
        lambda x: x.replace('_aq', '')[:10] + '_aq' if 'aq' in x else x)
    bj_df = bj_df[bj_df['station_id'].isin(bj_stations)]
    bj_df.set_index(['station_id', 'time'], inplace=True)

    filename = "ld_aq.csv"
    ld_df = pd.read_csv(
        os.path.join(config['label_dir'], filename),
        parse_dates=['MeasurementDateGMT']
    )
    ld_df.rename(columns={'MeasurementDateGMT': 'time'}, inplace=True)
    ld_df = ld_df[ld_df['station_id'].isin(ld_stations)]
    ld_df['O3'] = np.nan
    ld_df.set_index(['station_id', 'time'], inplace=True)

    label_df = pd.concat([bj_df, ld_df], axis=0)
    label_df[label_df < 0.] = np.nan

    data = {
        'keys': keys,
        'valid': valid_df,
        'test': test_df,
        'label': label_df,
    }
    return data


def to_submission(df_dict):
    result_df = pd.DataFrame()
    for air in ['PM2.5', 'PM10', 'O3']:
        df = pd.concat(df_dict[air], axis=0)
        result_df = pd.concat([result_df, df], axis=1)
    result_df.reset_index(['station_id', 'time', 'hour'],
                          drop=True, inplace=True)
    return result_df


def linear_ensemble2(valid_df, test_df, label_s, city_stations, method):
    valid_station_id_s = valid_df.index.get_level_values('station_id')
    test_station_id_s = test_df.index.get_level_values('station_id')

    # validation
    mask = valid_station_id_s.isin(city_stations)
    city_valid_df = valid_df[mask]
    city_valid_df.reset_index(
        ['submit_date', 'test_id'], drop=True, inplace=True)
    X_valid = city_valid_df.values
    y_valid = label_s.loc[city_valid_df.index].values

    if X_valid.ndim == 1:
        X_valid = X_valid.reshape((-1, 1))

    isnan = np.isnan(X_valid).any(axis=1) | np.isnan(y_valid)
    X_valid = X_valid[~isnan]
    y_valid = y_valid[~isnan]

    # testing
    mask = test_station_id_s.isin(city_stations)
    city_test_df = test_df[mask]
    X_test = city_test_df.values

    if X_test.ndim == 1:
        X_test = X_test.reshape((-1, 1))

    feature_importance = None
    if method == 'linear':
        regressor = LinearRegression()
        regressor.fit(X_valid, y_valid)
        feature_importance = regressor.coef_
    test_prediction = regressor.predict(X_test)
    valid_prediction = regressor.predict(X_valid)
    error = smape(y_valid, valid_prediction)

    df = pd.DataFrame(test_prediction, index=city_test_df.index)
    return df, error, feature_importance


def linear_ensemble(X_valid, y_valid, X_test):
    model = LinearRegression()
    model.fit(X_valid, y_valid)
    valid_pred = model.predict(X_valid)
    test_pred = model.predict(X_test)
    return valid_pred, test_pred


def split_prediction(df, city, split_days):
    stations = bj_stations if city == 'bj' else ld_stations
    station_mask = df.index.get_level_values('station_id').isin(stations)

    if split_days:
        first_day = (df.index.get_level_values('hour') < 24)
        first_mask = station_mask & first_day
        second_mask = station_mask & (~first_day)
        return [df[first_mask], df[second_mask]]
    else:
        mask = station_mask
        return [df[mask]]


def build_data(df, label_df=None):
    df[df.isnull() | (df < 0.)] = 0.
    X = df.values

    if X.ndim == 1:
        X = X.reshape((-1, 1))

    if label_df is not None:
        index = df.reset_index(['submit_date', 'test_id', 'hour']).index
        y = label_df.loc[index].values
        is_nan = np.isnan(y)
        X = X[~is_nan]
        y = y[~is_nan]
        return X, y, df[~is_nan].index
    else:
        return X, df.index


def compute_smape(df, label_df):
    df = df.reset_index(['submit_date', 'test_id', 'hour'], drop=True)
    df[df.isnull() | (df < 0.)] = 0.

    y_true = label_df.loc[df.index].values
    y_pred = df.values

    # ignore nan truth
    is_nan = np.isnan(y_true)
    score = smape(y_true[~is_nan], y_pred[~is_nan])
    return score


def weighted_ensemble(valid_df, test_df, weights):
    valid_df.columns = [
        '{}_{}'.format(name, i) for i, name in enumerate(valid_df.columns)]
    test_df.columns = [
        '{}_{}'.format(name, i) for i, name in enumerate(test_df.columns)]
    ens_valid_df = (valid_df * weights).mean(axis=1)
    ens_test_df = (test_df * weights).mean(axis=1)
    result = {
        'valid': ens_valid_df,
        'test': ens_test_df
    }
    return result


def ensemble(data, method, split_days):
    size = len(data['keys'])
    label_df = data['label']

    res_valid = defaultdict(list)
    res_test = defaultdict(list)
    scores = []
    targets = ['bj_PM2.5', 'bj_PM10', 'bj_O3', 'ld_PM2.5', 'ld_PM10']
    for target in targets:
        city, air = target.split('_')
        valid_dfs = split_prediction(data['valid'], city, split_days)
        test_dfs = split_prediction(data['test'], city, split_days)
        for i, (valid_df, test_df) in enumerate(zip(valid_dfs, test_dfs)):
            if method == 'uniform':
                weights = np.ones(size)
                result = weighted_ensemble(valid_df[air], test_df[air],
                                           weights)
            else:
                X_valid, y_valid, valid_index = build_data(
                    valid_df[air], label_df[air])
                X_test, test_index = build_data(test_df[air])
                if method == 'nn':
                    valid_pred, test_pred = nn_ensemble(
                        X_valid, y_valid, X_test)
                elif method == 'linear':
                    valid_pred, test_pred = linear_ensemble(
                        X_valid, y_valid, X_test)
                else:
                    raise Exception(
                        "Method {} is not supported".format(method))

                result = {
                    'valid': pd.Series(valid_pred, index=valid_index),
                    'test': pd.Series(test_pred, index=test_index)
                }

            res_valid[air].append(result['valid'].to_frame(air))
            res_test[air].append(result['test'].to_frame(air))
            score = compute_smape(result['valid'], label_df[air])
            logger.info(('{} ensemble '
                         '{}#{}: {:.6f}').format(method, target, i, score))
            scores.append(score)
    logger.info("Average SAMPE score: {}".format(np.mean(scores)))

    ens_valid_df = to_submission(res_valid)
    ens_test_df = to_submission(res_test)
    desc = "meth={},valid_score={:.6f}".format(method, np.mean(scores))
    return ens_valid_df, ens_test_df, desc


def get_args():
    methods = ['uniform', 'linear', 'nn']
    parser = argparse.ArgumentParser("KDDCup2018 ensemble")
    parser.add_argument("--config",
                        default='./config.yml',
                        help="configuration")
    parser.add_argument("-d", "--date", help="submission date")
    parser.add_argument("-m", "--method", default=methods[0], choices=methods)
    parser.add_argument("-s", "--split_days", action="store_true",
                        help="specified to split days")
    parser.add_argument("--upload", action="store_true",
                        help="specified to upload submission file")
    return parser.parse_args()


def main(args):
    # read configuration
    with open(args.config) as fp:
        config = yaml.load(fp)

    # read files
    logger.info("Loading files...")
    data = load_files(config, args.date)

    # ensemble prediction results
    logger.info("Running {} method...".format(args.method))
    # result_df, desc = ensemble(data, args.method)
    valid_df, test_df, desc = ensemble(data, args.method, args.split_days)
    # clip negative value
    test_df[test_df < 0.] = 1.
    valid_df[valid_df < 0.] = 1.

    time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
    path = os.path.join(
        config['test_output_dir'],
        "ens_{}_{}_{}.csv".format(args.date, args.method, time)
    )
    logger.info("Writing outputs to {}...".format(path))
    test_df.to_csv(path)

    # path = os.path.join(
        # config['valid_output_dir'],
        # "ens_{}_{}_{}.csv".format(args.date, args.method, time)
    # )
    # logger.info("Writing outputs to {}...".format(path))
    # valid_df.to_csv(path)

    # upload submission
    if args.upload:
        logger.info("Uploading submission file {}...".format(path))
        upload_submission(path, desc)


if __name__ == '__main__':
    args = get_args()
    logging.basicConfig(format='[%(asctime)s] %(message)s', level=logging.INFO)
    if args.method == 'nn':
        from nn import nn_ensemble
    main(args)
