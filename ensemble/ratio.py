import pandas as pd
import datetime
import argparse
import logging
import os
import json

def get_logger():
    logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--filepath', required=True, type=str)
    parser.add_argument('--city', default='bj', type=str)
    parser.add_argument('--start_hour', default=24, type=int)
    parser.add_argument('--end_hour', default=47, type=int)
    parser.add_argument('--PM25', default=1.2, type=float)
    parser.add_argument('--PM10', default=1.2, type=float)
    args = parser.parse_args()
    print(json.dumps(vars(args), indent=2, sort_keys=True))
    return args

args = parse_args()
logger = get_logger()

bj_station_list = [
        'aotizhongxin_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq',
       'dingling_aq', 'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq',
       'fangshan_aq', 'fengtaihuayuan_aq', 'guanyuan_aq', 'gucheng_aq',
       'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq',
       'miyunshuiku_aq', 'nansanhuan_aq', 'nongzhanguan_aq',
       'pingchang_aq', 'pinggu_aq', 'qianmen_aq', 'shunyi_aq',
       'tiantan_aq', 'tongzhou_aq', 'wanliu_aq', 'wanshouxigong_aq',
       'xizhimenbei_aq', 'yanqin_aq', 'yizhuang_aq', 'yongdingmennei_aq',
       'yongledian_aq', 'yufa_aq', 'yungang_aq', 'zhiwuyuan_aq',]

bj_station_list = [ x.replace('_aq','')[:10] + '_aq' if 'aq' in x else x for x in bj_station_list ]

ld_station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2',
                'GN0', 'KF1', 'CD9', 'ST5', 'TH4']

def apply_ratio(filepath, city, start_hour, end_hour, PM25_ratio, PM10_ratio):

    filename = os.path.basename(filepath)
    dirname = os.path.dirname(filepath)
    df = pd.read_csv(filepath)
    df['station_id'], df['hour'] = df['test_id'].str.split('#', 1).str
    df['hour'] = df['hour'].astype(int)

    ### multiply last 24 hour for ld
    station_list = bj_station_list if city == 'bj' else ld_station_list
    station_filter = df['station_id'].isin(station_list)
    hour_filter = ( (df['hour'] >= start_hour) & (df['hour'] <= end_hour))
    # PM25_ratio = 1.3
    # PM10_ratio = 1.5
    df.loc[station_filter & hour_filter, 'PM2.5'] = df.loc[station_filter & hour_filter, 'PM2.5'] * PM25_ratio
    df.loc[station_filter & hour_filter, 'PM10'] = df.loc[station_filter & hour_filter, 'PM10'] * PM10_ratio
    output_name = '{}_{}_{}.csv'.format(filename.replace('.csv',''), PM25_ratio, PM10_ratio )
    output_path = os.path.join(dirname, output_name)
    df[['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3']].to_csv(output_path, index=False)

if __name__ == '__main__':

    apply_ratio(args.filepath, args.city,
                args.start_hour, args.end_hour,
                args.PM25, args.PM10)


