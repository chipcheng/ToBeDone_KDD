import pandas as pd
import sys
import ipdb

def split_test_id(panda):
    panda['hour'] = [int(test_id.split('#')[1]) for test_id in panda['test_id']]
    panda['stationid'] = [test_id.split('#')[0] for test_id in panda['test_id']]
    return panda

bj_PM25_path = sys.argv[1]
bj_PM10_path = sys.argv[2]
bj_O3_path = sys.argv[3]
ld_PM25_path = sys.argv[4]
ld_PM10_path = sys.argv[5]
output_path = sys.argv[6]

station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4']
bj_PM25_panda = split_test_id(pd.read_csv(bj_PM25_path))
bj_PM10_panda = split_test_id(pd.read_csv(bj_PM10_path))
bj_O3_panda = split_test_id(pd.read_csv(bj_O3_path))
ld_PM25_panda = split_test_id(pd.read_csv(ld_PM25_path))
ld_PM10_panda = split_test_id(pd.read_csv(ld_PM10_path))

bj_PM25_panda = bj_PM25_panda[~bj_PM25_panda['stationid'].isin(station_list)][["PM2.5", "test_id", 'submit_date']]
bj_PM10_panda = bj_PM10_panda[~bj_PM10_panda['stationid'].isin(station_list)][["PM10", "test_id", 'submit_date']]
bj_O3_panda = bj_O3_panda[~bj_O3_panda['stationid'].isin(station_list)][["O3", "test_id", 'submit_date']]

bj_panda = pd.merge(bj_PM25_panda, bj_PM10_panda, on=['submit_date', 'test_id'])
bj_panda = pd.merge(bj_panda, bj_O3_panda, on=['submit_date', 'test_id'])

ld_PM25_panda = ld_PM25_panda[ld_PM25_panda['stationid'].isin(station_list)][["PM2.5", "test_id", 'submit_date']]
ld_PM10_panda = ld_PM10_panda[ld_PM10_panda['stationid'].isin(station_list)][["PM10", "test_id", 'submit_date']]
ld_panda = pd.merge(ld_PM25_panda, ld_PM10_panda, on=['submit_date','test_id'])

final_panda = pd.concat([bj_panda, ld_panda])
final_panda.to_csv(output_path, index=False, columns=["submit_date", "test_id", "PM2.5", "PM10", "O3"])