import numpy as np
np.random.seed(233)
import pandas as pd
import sys
import copy
import operator
import datetime
import pickle
import codecs
#sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt

from api_utils import download_latest_data, append_latest_data


data_dir = './data/'
aqi_beijing_csv = data_dir + 'beijing_aq_extend.csv'
aqi_london_csv = data_dir + 'london_aq_extend.csv'

bj_location_txt = data_dir + 'beijing_station_location.txt'
ld_location_txt = data_dir + 'london_station_location.txt'


beijing_id_list = ['yongdingmennei_aq', 'dingling_aq', 'shunyi_aq', 'dongsi_aq', 'fangshan_aq', 'miyun_aq', 'fengtaihuayuan_aq', 'donggaocun_aq', 'nansanhuan_aq', 'yanqin_aq', 'yongledian_aq', 'yizhuang_aq', 'pinggu_aq', 'xizhimenbei_aq', 'tongzhou_aq', 'dongsihuan_aq', 'yufa_aq', 'gucheng_aq', 'badaling_aq', 'zhiwuyuan_aq', 'daxing_aq', 'tiantan_aq', 'yungang_aq', 'beibuxinqu_aq', 'liulihe_aq', 'wanliu_aq', 'miyunshuiku_aq', 'huairou_aq', 'wanshouxigong_aq', 'pingchang_aq', 'mentougou_aq', 'nongzhanguan_aq', 'guanyuan_aq', 'qianmen_aq', 'aotizhongxin_aq']
london_id_list = ['MY7', 'KF1', 'CD1', 'TH4' ,'CD9', 'GR4', 'GR9', 'ST5', 'LW2', 'HV1', 'GN3', 'BL0', 'GN0']

bj_label_list = ['PM2.5', 'PM10', 'O3', 'NO2', 'CO', 'SO2']
ld_label_list = ['PM2.5', 'PM10', 'NO2']


fill_nan = np.nan
one_hour = datetime.timedelta(hours=1)
one_day = datetime.timedelta(days=1)
one_week = datetime.timedelta(weeks=1)
one_month = datetime.timedelta(days=30)


def read_bj_location_txt(location_txt):
    loc_dict = {}
    with open(location_txt, 'r') as f:
        for line in f:
            aq_id = line.rstrip().split(' ')[0]
            x, y = float(line.rstrip().split(' ')[1]), float(line.rstrip().split(' ')[2])
            loc_dict[aq_id] = (x, y)

    bj_neighbors = {}
    for aq_id in beijing_id_list:
        xi, yi = loc_dict[aq_id]

        neighbors = {}
        for neighbor in beijing_id_list:
            if aq_id != neighbor:
                xn, yn = loc_dict[neighbor]
                neighbors[neighbor] = np.square(xi-xn) + np.square(yi-yn)

        sorted_neighbors = sorted(neighbors.items(), key=operator.itemgetter(1))
        neighbors_list = [neighbor[0] for neighbor in sorted_neighbors]
        bj_neighbors[aq_id] = neighbors_list

    return bj_neighbors


def read_ld_location_txt(location_txt):
    loc_dict = {}
    with open(location_txt, 'r') as f:
        for line in f:
            aq_id = line.rstrip().split(' ')[0]
            x, y = float(line.rstrip().split(' ')[1]), float(line.rstrip().split(' ')[2])
            loc_dict[aq_id] = (x, y)

    ld_neighbors = {}
    for aq_id in london_id_list:
        xi, yi = loc_dict[aq_id]

        neighbors = {}
        for neighbor in london_id_list:
            if aq_id != neighbor:
                xn, yn = loc_dict[neighbor]
                neighbors[neighbor] = np.square(xi-xn) + np.square(yi-yn)

        sorted_neighbors = sorted(neighbors.items(), key=operator.itemgetter(1))
        neighbors_list = [neighbor[0] for neighbor in sorted_neighbors]
        ld_neighbors[aq_id] = neighbors_list

    return ld_neighbors



def read_bj_aq_csv(aqi_data_csv, location_id_list, begin_t, end_t):
    aqi_data = pd.read_csv(aqi_data_csv)

    aqi_id = aqi_data["stationId"].fillna("MissingID").values
    aqi_time = aqi_data["utc_time"].values
    aqi_pm25 = aqi_data["PM2.5"].fillna(fill_nan).values
    aqi_pm10 = aqi_data["PM10"].fillna(fill_nan).values
    aqi_o3 = aqi_data["O3"].fillna(fill_nan).values
    aqi_no2 = aqi_data["NO2"].fillna(fill_nan).values
    aqi_co = aqi_data["CO"].fillna(fill_nan).values
    aqi_so2 = aqi_data["SO2"].fillna(fill_nan).values

    # collect all valid values
    all_data = {}
    for target_id in location_id_list:
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(aqi_time[idx], '%Y-%m-%d %H:%M:%S')
                if now_t not in dict_pollut:
                    dict_pollut[now_t] = [float(aqi_pm25[idx]), float(aqi_pm10[idx]), float(aqi_o3[idx]), float(aqi_no2[idx]), float(aqi_co[idx]), float(aqi_so2[idx])]

        # fill every timestamp
        now_t = begin_t
        while True:
            if now_t not in dict_pollut:
                dict_pollut[now_t] = [float(fill_nan), float(fill_nan), float(fill_nan), float(fill_nan), float(fill_nan), float(fill_nan)]

            now_t += one_hour
            if now_t > end_t:
                break

        all_data[target_id] = dict_pollut

    return all_data


def read_ld_aq_csv(aqi_data_csv, location_id_list, begin_t, end_t):
    aqi_data = pd.read_csv(aqi_data_csv)

    aqi_id = aqi_data["Station_ID"].fillna("MissingID").values
    aqi_time = aqi_data["MeasurementDateGMT"].values
    aqi_pm25 = aqi_data["PM2.5"].fillna(fill_nan).values
    aqi_pm10 = aqi_data["PM10"].fillna(fill_nan).values
    aqi_no2 = aqi_data["NO2"].fillna(fill_nan).values

    # collect all valid values
    all_data = {}
    for target_id in location_id_list:
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(aqi_time[idx], '%Y/%m/%d %H:%M')
                if now_t not in dict_pollut:
                    dict_pollut[now_t] = [float(aqi_pm25[idx]), float(aqi_pm10[idx]), float(aqi_no2[idx])]

        # fill every timestamp
        now_t = begin_t
        while True:
            if now_t not in dict_pollut:
                dict_pollut[now_t] = [float(fill_nan), float(fill_nan), float(fill_nan)]

            now_t += one_hour
            if now_t > end_t:
                break

        all_data[target_id] = dict_pollut

    return all_data


def run_statistics(all_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    statistic_result = {}
    for label_idx, label in enumerate(label_list):
        all_nan_count = []
        all_date, all_count = 0, 0
        for target_id in location_id_list:
            all_value = []

            now_t = begin_t
            while True:
                all_value.append(all_data[target_id][now_t][label_idx])

                now_t += one_hour
                if now_t > end_t:
                    break

            binary_value = [1 if np.isnan(x) else 0 for x in all_value]

            nan_count = []
            count = 0
            for i in binary_value:
                if i == 1:
                    count += 1
                if i == 0 and count > 0:
                    nan_count.append(count)
                    count = 0
            if count > 0: nan_count.append(count)

            all_nan_count += nan_count
            all_date += len(binary_value)
            all_count += np.sum(binary_value)
            print (nan_count)
            sys.exit()

        statistic_result[label] = (all_count/all_date, np.mean(all_nan_count), np.std(all_nan_count))

    return statistic_result


def create_bj_missing(bj_data, location_id_list, begin_t, end_t):
    bj_missing_data = copy.deepcopy(bj_data)

    for target_id in location_id_list:
        for label_idx, label in enumerate(bj_label_list):
            now_t = begin_t

            while True:
                while True:
                    if np.random.rand(1)[0] > 0.025:
                        now_t += one_hour
                    else:
                        break

                if now_t > end_t: break

                while True:
                    c = np.random.poisson(1, 1)[0]
                    if c > 0: break

                if np.random.rand(1)[0] < 0.008:
                    c += int(np.random.uniform(0,400,1)[0])

                while c > 0:
                    bj_missing_data[target_id][now_t][label_idx] = float(fill_nan)
                    now_t += one_hour
                    c -= 1
                    if now_t > end_t: break

                if now_t > end_t: break

    # save dictionary
    data = {'bj_data': bj_data, 'bj_missing_data': bj_missing_data}
    with open(data_dir + 'bj_fill.pkl', 'wb') as f:
        pickle.dump(data, f)

    return bj_missing_data


def test_bj_simulate_missing():
    truth = [0]*20000

    idx = 0
    while True:
        while True:
            if np.random.rand(1)[0] > 0.025:
                idx += 1
            else:
                break

        if idx >= len(truth): break

        while True:
            c = np.random.poisson(1, 1)[0]
            if c > 0: break

        if np.random.rand(1)[0] < 0.008:
            c += int(np.random.uniform(0,400,1)[0])

        while c > 0:
            truth[idx] = 1
            idx += 1
            c -= 1
            if idx >= len(truth): break

        if idx >= len(truth): break

    nan_count = []
    count = 0
    for i in truth:
        if i == 1:
            count += 1
        if i == 0 and count > 0:
            nan_count.append(count)
            count = 0
    if count > 0: nan_count.append(count)

    all_nan_count = nan_count
    all_date = len(truth)
    all_count = np.sum(truth)
    print (nan_count)
    print (all_count/all_date, np.mean(all_nan_count), np.std(all_nan_count))


def create_ld_missing(ld_data, location_id_list, begin_t, end_t):
    ld_missing_data = copy.deepcopy(ld_data)

    for target_id in location_id_list:
        for label_idx, label in enumerate(ld_label_list):
            now_t = begin_t

            while True:
                while True:
                    if np.random.rand(1)[0] > 0.02:
                        now_t += one_hour
                    else:
                        break

                if now_t > end_t: break

                while True:
                    c = np.random.poisson(1, 1)[0]
                    if c > 0: break

                if np.random.rand(1)[0] < 0.05:
                    c += int(np.random.uniform(5,100,1)[0])
                if np.random.rand(1)[0] < 0.006:
                    c += int(np.random.uniform(100,1000,1)[0])

                while c > 0:
                    ld_missing_data[target_id][now_t][label_idx] = float(fill_nan)
                    now_t += one_hour
                    c -= 1
                    if now_t > end_t: break

                if now_t > end_t: break

    # save dictionary
    data = {'ld_data': ld_data, 'ld_missing_data': ld_missing_data}
    with open(data_dir + 'ld_fill.pkl', 'wb') as f:
        pickle.dump(data, f)

    return ld_missing_data


def test_ld_simulate_missing():
    truth = [0]*20000

    idx = 0
    while True:
        while True:
            if np.random.rand(1)[0] > 0.02:
                idx += 1
            else:
                break

        if idx >= len(truth): break

        while True:
            c = np.random.poisson(1, 1)[0]
            if c > 0: break

        if np.random.rand(1)[0] < 0.05:
            c += int(np.random.uniform(5,100,1)[0])
        if np.random.rand(1)[0] < 0.006:
            c += int(np.random.uniform(100,1000,1)[0])

        while c > 0:
            truth[idx] = 1
            idx += 1
            c -= 1
            if idx >= len(truth): break

        if idx >= len(truth): break

    nan_count = []
    count = 0
    for i in truth:
        if i == 1:
            count += 1
        if i == 0 and count > 0:
            nan_count.append(count)
            count = 0
    if count > 0: nan_count.append(count)

    all_nan_count = nan_count
    all_date = len(truth)
    all_count = np.sum(truth)
    print (nan_count)
    print (all_count/all_date, np.mean(all_nan_count), np.std(all_nan_count))



def fill_with_prev_hour(city_missing_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    for target_id in location_id_list:
        for label_idx, label in enumerate(label_list):
            now_t = begin_t
            if np.isnan(city_missing_data[target_id][now_t][label_idx]): city_missing_data[target_id][now_t][label_idx] = 0.1

            while True:
                if np.isnan(city_missing_data[target_id][now_t][label_idx]):
                    city_missing_data[target_id][now_t][label_idx] = city_missing_data[target_id][now_t-one_hour][label_idx]

                now_t += one_hour
                if now_t > end_t:
                    break

    return city_missing_data


def fill_with_zero(city_missing_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    for target_id in location_id_list:
        for label_idx, label in enumerate(label_list):
            now_t = begin_t

            while True:
                if np.isnan(city_missing_data[target_id][now_t][label_idx]):
                    city_missing_data[target_id][now_t][label_idx] = 0

                now_t += one_hour
                if now_t > end_t:
                    break

    return city_missing_data


def fill_with_prev_smartly(city_missing_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    city_fill_data = copy.deepcopy(city_missing_data)

    for target_id in location_id_list:
        for label_idx, label in enumerate(label_list):
            now_t = begin_t

            while True:
                if np.isnan(city_fill_data[target_id][now_t][label_idx]):
                    if now_t-one_hour >= begin_t and not np.isnan(city_missing_data[target_id][now_t-one_hour][label_idx]):
                        city_fill_data[target_id][now_t][label_idx] = city_missing_data[target_id][now_t-one_hour][label_idx]

                    elif now_t-one_day >= begin_t and not np.isnan(city_missing_data[target_id][now_t-one_day][label_idx]):
                        city_fill_data[target_id][now_t][label_idx] = city_missing_data[target_id][now_t-one_day][label_idx]

                    elif now_t-one_week >= begin_t and not np.isnan(city_missing_data[target_id][now_t-one_week][label_idx]):
                        city_fill_data[target_id][now_t][label_idx] = city_missing_data[target_id][now_t-one_week][label_idx]

                    elif now_t-one_month >= begin_t and not np.isnan(city_missing_data[target_id][now_t-one_month][label_idx]):
                        city_fill_data[target_id][now_t][label_idx] = city_missing_data[target_id][now_t-one_month][label_idx]

                now_t += one_hour
                if now_t > end_t:
                    break

    return city_fill_data


def fill_with_interpolation(city_missing_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list


    def nan_helper(y):
        '''
        Helper to handle indices and logical indices of NaNs.

        Input:
            - y, 1d numpy array with possible NaNs
        Output:
            - nans, logical indices of NaNs
            - index, a function, with signature indices= index(logical_indices),
              to convert logical indices of NaNs to 'equivalent' indices
        Example:
            >>> # linear interpolation of NaNs
            >>> nans, x= nan_helper(y)
            >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
        '''
        return np.isnan(y), lambda z: z.nonzero()[0]


    for target_id in location_id_list:
        for label_idx, label in enumerate(label_list):
            y = []
            now_t = begin_t
            if np.isnan(city_missing_data[target_id][now_t][label_idx]): city_missing_data[target_id][now_t][label_idx] = 10

            while True:
                y.append(city_missing_data[target_id][now_t][label_idx])

                now_t += one_hour
                if now_t > end_t:
                    break

            y = np.array(y)
            nans, x = nan_helper(y)
            y[nans]= np.interp(x(nans), x(~nans), y[~nans])

            now_t = begin_t
            y_idx = 0
            while True:
                city_missing_data[target_id][now_t][label_idx] = y[y_idx]

                now_t += one_hour
                y_idx += 1
                if now_t > end_t:
                    break

    return city_missing_data


def fill_with_knn(city_missing_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    neighbors = {}
    if city == 'bj': neighbors = read_bj_location_txt(bj_location_txt)
    if city == 'ld': neighbors = read_ld_location_txt(ld_location_txt)


    city_fill_data = copy.deepcopy(city_missing_data)

    def get_5_neighbor(target_id, now_t, label_idx):
        nei_value = []
        for nei_idx, neighbor in enumerate(neighbors[target_id]):
            #if now_t in all_data[neighbor]:
            if not np.isnan(city_missing_data[neighbor][now_t][label_idx]):
                nei_value.append(city_missing_data[neighbor][now_t][label_idx])

            if nei_idx >= 4:
                break

        if len(nei_value) >= 1:
            return np.mean(nei_value)
        else:
            return float(fill_nan)


    for target_id in location_id_list:
        for label_idx, label in enumerate(label_list):
            now_t = begin_t

            while True:
                if np.isnan(city_missing_data[target_id][now_t][label_idx]):
                    city_fill_data[target_id][now_t][label_idx] = get_5_neighbor(target_id, now_t, label_idx)

                now_t += one_hour
                if now_t > end_t:
                    break

    return city_fill_data



def compare_smape(city_missing_data, city_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    for label_idx, label in enumerate(label_list):
        smapes_id = []
        for target_id in location_id_list:
            smapes_time = []
            now_t = begin_t
            while True:
                if not np.isnan(city_data[target_id][now_t][label_idx]):
                    smapes_time.append(symmetric_mean_absolute_percentage_error(city_missing_data[target_id][now_t][label_idx], city_data[target_id][now_t][label_idx]))

                now_t += one_hour
                if now_t > end_t:
                    break

            if smapes_time != []:
                smapes_id.append(np.mean(smapes_time))
            else:
                smapes_id.append(0)

        print (label)
        print (np.mean(smapes_id))
        print ('')


def symmetric_mean_absolute_percentage_error(y_pred, y_true):
    if np.abs(y_true) + np.abs(y_pred) == 0:
        return 0
    else:
        return 2*np.abs(y_true - y_pred) / (np.abs(y_true)+np.abs(y_pred)) * 100


def create_csv(city_data, location_id_list, begin_t, end_t, city):
    label_list = []
    if city == 'bj': label_list = bj_label_list
    if city == 'ld': label_list = ld_label_list

    with open('./data/{}_knn_interp.csv'.format(city), 'w') as f:
        f.write('id,station_id,time,PM25_Concentration,PM10_Concentration,NO2_Concentration,CO_Concentration,O3_Concentration,SO2_Concentration\n')
        for target_id in location_id_list:
            now_t = begin_t

            while True:
                if city == 'bj':
                    f.write('0,{},'.format(target_id))
                    f.write('{},'.format(now_t.strftime("%Y-%m-%d %H:%M:%S")))
                    f.write('{},'.format(round(city_data[target_id][now_t][0], 2)))
                    f.write('{},'.format(round(city_data[target_id][now_t][1], 2)))
                    f.write('{},'.format(round(city_data[target_id][now_t][3], 2)))
                    f.write('{},'.format(round(city_data[target_id][now_t][4], 2)))
                    f.write('{},'.format(round(city_data[target_id][now_t][2], 2)))
                    f.write('{}\n'.format(round(city_data[target_id][now_t][5], 2)))

                if city == 'ld':
                    f.write('0,{},'.format(target_id))
                    f.write('{},'.format(now_t.strftime("%Y-%m-%d %H:%M:%S")))
                    f.write('{},'.format(round(city_data[target_id][now_t][0], 2)))
                    f.write('{},'.format(round(city_data[target_id][now_t][1], 2)))
                    f.write('{},'.format(round(city_data[target_id][now_t][2], 2)))
                    f.write('{},'.format(''))
                    f.write('{},'.format(''))
                    f.write('{}\n'.format(''))


                now_t += one_hour
                if now_t > end_t:
                    break

def get_latest_end_t():
    bj_data = pd.read_csv(aqi_beijing_csv)
    bj_time = bj_data["utc_time"].values
    end_t_bj = datetime.datetime.strptime(bj_time[-1], '%Y-%m-%d %H:%M:%S')

    ld_data = pd.read_csv(aqi_london_csv)
    ld_time = ld_data["MeasurementDateGMT"].values
    end_t_ld = datetime.datetime.strptime(ld_time[-1], '%Y/%m/%d %H:%M')

    end_t_bj = datetime.datetime.strptime('{} 23:00:00'.format(end_t_bj.strftime("%Y-%m-%d")), '%Y-%m-%d %H:%M:%S')
    end_t_ld = datetime.datetime.strptime('{} 23:00:00'.format(end_t_ld.strftime("%Y-%m-%d")), '%Y-%m-%d %H:%M:%S')

    return end_t_bj, end_t_ld


if __name__ == '__main__':

    if sys.argv[1] == '--download_latest':
        download_latest_data(begin_time='2017-01-01-0', end_time='2018-07-31-0')
        append_latest_data()

    elif sys.argv[1] == '--create_csv':
        begin_t_bj = datetime.datetime.strptime('2017-01-01 14:00:00', '%Y-%m-%d %H:%M:%S')
        begin_t_ld = datetime.datetime.strptime('2017-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        end_t_bj, end_t_ld = get_latest_end_t()


        bj_data = read_bj_aq_csv(aqi_beijing_csv, beijing_id_list, begin_t_bj, end_t_bj)
        ld_data = read_ld_aq_csv(aqi_london_csv, london_id_list, begin_t_ld, end_t_ld)


        bj_data = fill_with_knn(bj_data, beijing_id_list, begin_t_bj, end_t_bj, city='bj')
        ld_data = fill_with_knn(ld_data, london_id_list, begin_t_ld, end_t_ld, city='ld')

        bj_data = fill_with_interpolation(bj_data, beijing_id_list, begin_t_bj, end_t_bj, city='bj')
        ld_data = fill_with_interpolation(ld_data, london_id_list, begin_t_ld, end_t_ld, city='ld')


        create_csv(bj_data, beijing_id_list, begin_t_bj, end_t_bj, city='bj')
        create_csv(ld_data, london_id_list, begin_t_ld, end_t_ld, city='ld')
