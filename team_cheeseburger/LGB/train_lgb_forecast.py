import numpy as np
import datetime
import sys
import os

#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt

#import paramiko
#from scp import SCPClient # https://github.com/jbardin/scp.py

from sklearn.ensemble import RandomForestRegressor
import lightgbm as lgb
from sklearn.externals import joblib


from pre_process_beijing_forecast import read_bj_aq_csv, get_bj_aq_data, read_bj_forecast_csv, read_bj_location_txt, get_bj_aq_data_daily
from pre_process_london_forecast import read_ld_aq_csv, get_ld_aq_data, read_ld_forecast_csv, read_ld_location_txt, get_ld_aq_data_daily
#from api_utils import get_beijing_aq_with_api, get_london_aq_with_api
#from api_utils import download_latest_data, append_latest_data


data_dir = './data/'
model_aq_dir = './model_aq/'
model_forecast_dir = './model_forecast/'
#source_dir = '/dhome/Jhopo/KDD_CUP/'

bj_location_txt = data_dir + 'beijing_station_location.txt'
ld_location_txt = data_dir + 'london_station_location.txt'

aqi_beijing_csv = data_dir + 'bj_knn_interp.csv'
aqi_london_csv = data_dir + 'ld_knn_interp.csv'

forecast_beijing_csv = data_dir + 'forecast/bj_hourly_worldweather.csv'
forecast_london_csv = data_dir + 'forecast/ld_hourly_worldweather.csv'

meo_london_grid_csv = data_dir + 'London_historical_meo_grid.csv'

beijing_id_list = ['yongdingmennei_aq', 'dingling_aq', 'shunyi_aq', 'dongsi_aq', 'fangshan_aq', 'miyun_aq', 'fengtaihuayuan_aq', 'donggaocun_aq', 'nansanhuan_aq', 'yanqin_aq', 'yongledian_aq', 'yizhuang_aq', 'pinggu_aq', 'xizhimenbei_aq', 'tongzhou_aq', 'dongsihuan_aq', 'yufa_aq', 'gucheng_aq', 'badaling_aq', 'zhiwuyuan_aq', 'daxing_aq', 'tiantan_aq', 'yungang_aq', 'beibuxinqu_aq', 'liulihe_aq', 'wanliu_aq', 'miyunshuiku_aq', 'huairou_aq', 'wanshouxigong_aq', 'pingchang_aq', 'mentougou_aq', 'nongzhanguan_aq', 'guanyuan_aq', 'qianmen_aq', 'aotizhongxin_aq']

london_id_list = ['MY7', 'KF1', 'CD1', 'TH4' ,'CD9', 'GR4', 'GR9', 'ST5', 'LW2', 'HV1', 'GN3', 'BL0', 'GN0']

bj_label_list = ['PM2.5', 'PM10', 'O3']
ld_label_list = ['PM2.5', 'PM10']

one_day = datetime.timedelta(days=1)


def symmetric_mean_absolute_percentage_error(y_true, y_pred):
    return np.mean(2*np.abs(y_true - y_pred) / (np.abs(y_true)+np.abs(y_pred))) * 100


def train_lgb(X_train, y_train, X_test, y_test):
    '''
    lgb_train = lgb.Dataset(X_train, y_train)
    lgb_eval = lgb.Dataset(X_test, y_test, reference=lgb_train)
    # specify your configurations as a dict
    params = {
        'task': 'train',
        'boosting_type': 'gbdt',
        'objective': 'regression',
        'metric': {'l2', 'auc'},
        'num_leaves': 31,
        'learning_rate': 0.05,
        'feature_fraction': 0.9,
        'bagging_fraction': 0.8,
        'bagging_freq': 5,
        'verbose': 0
        'n_estimators': 100
    }

    print('Start training...')
    # train
    gbm = lgb.train(params,
                    lgb_train,
                    num_boost_round=20,
                    valid_sets=lgb_eval,
                    early_stopping_rounds=5)
    '''

    gbm_48 = []

    for hour_idx, y in enumerate(y_train):
        gbm = lgb.LGBMRegressor(objective='regression',
                                num_leaves=15,
                                learning_rate=0.1,
                                n_estimators=200,
                                random_state=442)
        gbm.fit(X_train, y,
                eval_set=[(X_test, y_test[hour_idx])],
                eval_metric='l1',
                early_stopping_rounds=5)

        gbm_48.append(gbm)

    return gbm_48

'''
def predict_and_scoring_test(gbm_pm25, gbm_pm10, gbm_no2, X_test, y_test_pm25, y_test_pm10, y_test_no2):
    pm25_all_smape = []
    pm10_all_smape = []
    no2_all_smape = []
    for idx_day in range(60):
        x = X_test[idx_day*24]

        pm25_pred_48h = []
        pm10_pred_48h = []
        no2_pred_48h = []
        for idx_hour in range(48):
            pm25_pred = gbm_pm25.predict(x.reshape(1, -1), num_iteration=gbm_pm25.best_iteration_)[0]
            pm10_pred = gbm_pm10.predict(x.reshape(1, -1), num_iteration=gbm_pm10.best_iteration_)[0]
            no2_pred = gbm_no2.predict(x.reshape(1, -1), num_iteration=gbm_no2.best_iteration_)[0]

            pm25_pred_48h.append(pm25_pred)
            pm10_pred_48h.append(pm10_pred)
            no2_pred_48h.append(no2_pred)

            x = np.append(x[3:], [pm25_pred,pm10_pred,no2_pred])

        pm25_true_48h = y_test_pm25[idx_day*24:idx_day*24+48]
        pm10_true_48h = y_test_pm10[idx_day*24:idx_day*24+48]
        no2_true_48h = y_test_no2[idx_day*24:idx_day*24+48]

        pm25_all_smape.append(symmetric_mean_absolute_percentage_error(pm25_true_48h, pm25_pred_48h))
        pm10_all_smape.append(symmetric_mean_absolute_percentage_error(pm10_true_48h, pm10_pred_48h))
        no2_all_smape.append(symmetric_mean_absolute_percentage_error(no2_true_48h, no2_pred_48h))

    print ('SMAPE pm2.5 : {}'.format(np.mean(pm25_all_smape)))
    print ('SMAPE pm10 : {}'.format(np.mean(pm10_all_smape)))
    print ('SMAPE NO2 : {}'.format(np.mean(no2_all_smape)))
'''

def predict_on_internal(gbm_pm25, gbm_pm10, gbm_O3, X_test, t_test):
    id_predict = {}
    for idx_day in range(len(t_test)):
        x = X_test[idx_day]
        t = t_test[idx_day]

        pm25_pred_48h = []
        pm10_pred_48h = []
        O3_pred_48h = []
        for idx_hour in range(48):
            pm25_pred = gbm_pm25[idx_hour].predict(x.reshape(1, -1), num_iteration=gbm_pm25[idx_hour].best_iteration_)[0]
            pm10_pred = gbm_pm10[idx_hour].predict(x.reshape(1, -1), num_iteration=gbm_pm10[idx_hour].best_iteration_)[0]
            O3_pred = gbm_O3[idx_hour].predict(x.reshape(1, -1), num_iteration=gbm_O3[idx_hour].best_iteration_)[0]

            pm25_pred_48h.append(pm25_pred)
            pm10_pred_48h.append(pm10_pred)
            O3_pred_48h.append(O3_pred)

        id_predict[t] = (pm25_pred_48h, pm10_pred_48h, O3_pred_48h)

    return id_predict


def generate_internal_submission(beijing_predict, londond_predict, t_test):
    output_fd = open('./internal.csv', 'w')
    output_fd.write('submit_date,test_id,PM2.5,PM10,O3\n')

    for idx_day in range(len(t_test)):
        t = t_test[idx_day]
        for location_id in beijing_id_list:
            pm25_pred_48h, pm10_pred_48h, o3_pred_48h = beijing_predict[location_id][t]
            for hour in range(48):
                if len(location_id) > 13: location_id = location_id[:10] + '_aq'
                output_fd.write('{},{}#{},{},{},{}\n'.format((t-one_day).strftime("%Y-%m-%d"), location_id, hour, pm25_pred_48h[hour], pm10_pred_48h[hour], o3_pred_48h[hour]))

        for location_id in london_id_list:
            pm25_pred_48h, pm10_pred_48h, _ = londond_predict[location_id][t]
            for hour in range(48):
                output_fd.write('{},{}#{},{},{},0\n'.format((t-one_day).strftime("%Y-%m-%d"), location_id, hour, pm25_pred_48h[hour], pm10_pred_48h[hour]))


def generate_external_submission(beijing_predict, londond_predict, t_test):
    output_fd = open('./external.csv', 'w')
    output_fd.write('submit_date,test_id,PM2.5,PM10,O3\n')

    for idx_day in range(len(t_test)):
        t = t_test[idx_day]
        for location_id in beijing_id_list:
            pm25_pred_48h, pm10_pred_48h, o3_pred_48h = beijing_predict[location_id][t]
            for hour in range(48):
                if len(location_id) > 13: location_id = location_id[:10] + '_aq'
                output_fd.write('{},{}#{},{},{},{}\n'.format((t-one_day).strftime("%Y-%m-%d"), location_id, hour, pm25_pred_48h[hour], pm10_pred_48h[hour], o3_pred_48h[hour]))

        for location_id in london_id_list:
            pm25_pred_48h, pm10_pred_48h, _ = londond_predict[location_id][t]
            for hour in range(48):
                output_fd.write('{},{}#{},{},{},0\n'.format((t-one_day).strftime("%Y-%m-%d"), location_id, hour, pm25_pred_48h[hour], pm10_pred_48h[hour]))



def save_gbm(target_id, gbm_48, label, is_forecast):
    if is_forecast == True:
        model_dir = model_aq_dir
    else:
        model_dir = model_forecast_dir

    for idx, gbm in enumerate(gbm_48):
        joblib.dump(gbm, model_dir + '{}_{}_{}.pkl'.format(target_id, label, idx))
        #gbm.save_model('./model/{}_{}_{}.txt'.format(target_id, label, gbm_48))

def load_gbm(target_id, label, is_forecast):
    if is_forecast == True:
        model_dir = model_aq_dir
    else:
        model_dir = model_forecast_dir

    gbm_48 = []
    for idx in range(48):
        gbm = joblib.load(model_dir + '{}_{}_{}.pkl'.format(target_id, label, idx))
        gbm_48.append(gbm)

    return gbm_48


def train_on_internal(predict_date, is_forecast):

    bj_data = read_bj_aq_csv(aqi_beijing_csv, beijing_id_list)
    forecast_all = read_bj_forecast_csv(forecast_beijing_csv)
    beijing_predict = {}
    for target_id in beijing_id_list:
        print('Start training pm2.5...')
        X_train_pm25, y_train_pm25, t_train_pm25, X_valid_pm25, y_valid_pm25, t_valid_pm25, X_test_pm25, t_test_pm25 = get_bj_aq_data(predict_date, target_id, bj_data, forecast_all, 'pm25', is_forecast)
        gbm_pm25 = train_lgb(X_train_pm25, y_train_pm25, X_valid_pm25, y_valid_pm25)
        save_gbm(target_id, gbm_pm25, 'pm25', is_forecast)

        print('Start training pm10...')
        X_train_pm10, y_train_pm10, t_train_pm10, X_valid_pm10, y_valid_pm10, t_valid_pm10, X_test_pm10, t_test_pm10 = get_bj_aq_data(predict_date, target_id, bj_data, forecast_all, 'pm10', is_forecast)
        gbm_pm10 = train_lgb(X_train_pm10, y_train_pm10, X_valid_pm10, y_valid_pm10)
        save_gbm(target_id, gbm_pm10, 'pm10', is_forecast)

        print('Start training O3...')
        X_train_O3, y_train_O3, t_train_O3, X_valid_O3, y_valid_O3, t_valid_O3, X_test_O3, t_test_O3 = get_bj_aq_data(predict_date, target_id, bj_data, forecast_all, 'o3', is_forecast)
        gbm_O3 = train_lgb(X_train_O3, y_train_O3, X_valid_O3, y_valid_O3)
        save_gbm(target_id, gbm_O3, 'o3', is_forecast)

        print('Start predicting...')
        id_predict = predict_on_internal(gbm_pm25, gbm_pm10, gbm_O3, X_test_pm25, t_test_pm25)
        beijing_predict[target_id] = id_predict


    ld_data = read_ld_aq_csv(aqi_london_csv, london_id_list)
    forecast_all = read_ld_forecast_csv(forecast_london_csv)
    london_predict = {}
    for target_id in london_id_list:
        print('Start training pm2.5...')
        X_train_pm25, y_train_pm25, t_train_pm25, X_valid_pm25, y_valid_pm25, t_valid_pm25, X_test_pm25, t_test_pm25 = get_ld_aq_data(predict_date, target_id, ld_data, forecast_all, 'pm25', is_forecast)
        gbm_pm25 = train_lgb(X_train_pm25, y_train_pm25, X_valid_pm25, y_valid_pm25)
        save_gbm(target_id, gbm_pm25, 'pm25', is_forecast)

        print('Start training pm10...')
        X_train_pm10, y_train_pm10, t_train_pm10, X_valid_pm10, y_valid_pm10, t_valid_pm10, X_test_pm10, t_test_pm10 = get_ld_aq_data(predict_date, target_id, ld_data, forecast_all, 'pm10', is_forecast)
        gbm_pm10 = train_lgb(X_train_pm10, y_train_pm10, X_valid_pm10, y_valid_pm10)
        save_gbm(target_id, gbm_pm10, 'pm10', is_forecast)

        gbm_O3 = gbm_pm25

        print('Start predicting...')
        id_predict = predict_on_internal(gbm_pm25, gbm_pm10, gbm_O3, X_test_pm25, t_test_pm25)
        london_predict[target_id] = id_predict


    generate_internal_submission(beijing_predict, london_predict, t_test_pm25)


def predict_daily(predict_date, is_forecast):
    bj_data = read_bj_aq_csv(aqi_beijing_csv, beijing_id_list)
    forecast_all = read_bj_forecast_csv(forecast_beijing_csv)
    beijing_predict = {}
    for target_id in beijing_id_list:
        gbm_pm25 = load_gbm(target_id, 'pm25', is_forecast)
        gbm_pm10 = load_gbm(target_id, 'pm10', is_forecast)
        gbm_O3 = load_gbm(target_id, 'o3', is_forecast)

        X_test, t_test = get_bj_aq_data_daily(predict_date, target_id, bj_data, forecast_all, 'pm25', is_forecast)
        id_predict = predict_on_internal(gbm_pm25, gbm_pm10, gbm_O3, X_test, t_test)
        beijing_predict[target_id] = id_predict


    ld_data = read_ld_aq_csv(aqi_london_csv, london_id_list)
    forecast_all = read_ld_forecast_csv(forecast_london_csv)
    london_predict = {}
    for target_id in london_id_list:
        gbm_pm25 = load_gbm(target_id, 'pm25', is_forecast)
        gbm_pm10 = load_gbm(target_id, 'pm10', is_forecast)
        gbm_O3 = gbm_pm25

        X_test, t_test = get_ld_aq_data_daily(predict_date, target_id, ld_data, forecast_all, 'pm25', is_forecast)
        id_predict = predict_on_internal(gbm_pm25, gbm_pm10, gbm_O3, X_test, t_test)
        london_predict[target_id] = id_predict

    generate_external_submission(beijing_predict, london_predict, t_test)

'''
def createSSHClient(server='140.112.31.185', port='22', user='Jhopo', password=''):
    with open('/dhome/Jhopo/password', 'r') as f:
        password = f.readline().rstrip()

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client
'''

if __name__ == '__main__':
    one_day = datetime.timedelta(days=1)

    #predict_date = datetime.datetime.now()
    #predict_date = datetime.datetime.strptime('2018-05-22 00:00:00', '%Y-%m-%d %H:%M:%S')
    what_date = sys.argv[2]
    predict_date = datetime.datetime.strptime('2018-' + what_date, '%Y-%m-%d')
    predict_date = predict_date + one_day
    print ('predict_date: {}'.format(predict_date.strftime("%Y-%m-%d")))

    '''
    Aq only
    '''
    # execute at 00:30 am
    if sys.argv[1] == '--train_aq':
        yesterday = predict_date - one_day
        os.system('rm -rf ./bj_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/bj_hourly_worldweather.csv'.format(yesterday.strftime("%m-%d")))
        os.system('cp -f ./bj_hourly_worldweather.csv ./data/forecast/')
        os.system('rm -rf ./ld_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/ld_hourly_worldweather.csv'.format(yesterday.strftime("%m-%d")))
        os.system('cp -f ./ld_hourly_worldweather.csv ./data/forecast/')


        train_on_internal(predict_date, is_forecast=False)

        os.system('mv ./internal.csv ../../output/valid/valid_{}_04.csv'.format(yesterday.strftime("%m-%d")))


    # execute at 07:25 am
    if sys.argv[1] == '--predict_aq':
        yesterday = predict_date - one_day
        os.system('rm -rf ./bj_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/bj_hourly_worldweather.csv'.format(predict_date.strftime("%m-%d")))
        os.system('cp -f ./bj_hourly_worldweather.csv ./data/forecast/')
        os.system('rm -rf ./ld_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/ld_hourly_worldweather.csv'.format(predict_date.strftime("%m-%d")))
        os.system('cp -f ./ld_hourly_worldweather.csv ./data/forecast/')


        predict_daily(predict_date, is_forecast=False)

        os.system('mv ./external.csv ../../output/test/test_{}_04.csv'.format(yesterday.strftime("%m-%d")))


    '''
    Forecast
    '''
    # execute at 00:30 am
    if sys.argv[1] == '--train_forecast':
        yesterday = predict_date - one_day
        os.system('rm -rf ./bj_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/bj_hourly_worldweather.csv'.format(yesterday.strftime("%m-%d")))
        os.system('cp -f ./bj_hourly_worldweather.csv ./data/forecast/')
        os.system('rm -rf ./ld_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/ld_hourly_worldweather.csv'.format(yesterday.strftime("%m-%d")))
        os.system('cp -f ./ld_hourly_worldweather.csv ./data/forecast/')


        train_on_internal(predict_date, is_forecast=True)

        os.system('mv ./internal.csv ../../output/valid/valid_{}_10.csv'.format(yesterday.strftime("%m-%d")))


    # execute at 07:25 am
    if sys.argv[1] == '--predict_forecast':
        yesterday = predict_date - one_day
        os.system('rm -rf ./bj_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/bj_hourly_worldweather.csv'.format(predict_date.strftime("%m-%d")))
        os.system('cp -f ./bj_hourly_worldweather.csv ./data/forecast/')
        os.system('rm -rf ./ld_hourly_worldweather.csv')
        os.system('wget https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-{}/ld_hourly_worldweather.csv'.format(predict_date.strftime("%m-%d")))
        os.system('cp -f ./ld_hourly_worldweather.csv ./data/forecast/')


        predict_daily(predict_date, is_forecast=True)

        os.system('mv ./external.csv ../../output/test/test_{}_10.csv'.format(yesterday.strftime("%m-%d")))
