import numpy as np
import pandas as pd
import datetime
import os
KDD=False
depth = 4
n_estimators = 15
import sys

desire_date = sys.argv[3]


mm = int(desire_date.split('-')[0])
dd = int(desire_date.split('-')[-1])
x = datetime.datetime(2018, mm, dd)

is_valid = (True if int(sys.argv[1])==1 else False)

import sys
m_now = x.month
d_now = x.day

meo_range = range(1,2)

if is_valid:

    end = x - datetime.timedelta(days=2)
    m_end = end.month
    d_end = end.day

    m_start = 3
    d_start = 31
else:
    
    end = x + datetime.timedelta(days=1)
    m_end = end.month
    d_end = end.day

    m_start = m_now
    d_start = d_now

    #print(desire_date, 'm_start', m_start, ', d_start', d_start)

#print(m_start, d_start, m_end, d_end)

if is_valid:
    f_name = 'valid/valid_' + str(m_now).zfill(2) + '-' + str(d_now).zfill(2) +'_09.csv'
else:
    f_name = 'test/test_' + str(m_now).zfill(2) + '-' + str(d_now).zfill(2) +'_09.csv'

print('f_name = ', f_name)
#m_end=int(sys.argv[3])
#d_end=int(sys.argv[4])
#m_start=int(sys.argv[1])
#d_start=int(sys.argv[2])


aq_pm25 = np.load('npy/aq_pm25_bj_new.npy').item()
aq_pm10 = np.load('npy/aq_pm10_bj_new.npy').item()
aq_o3 = np.load('npy/aq_o3_bj_new.npy').item()

meo_bj = np.load('npy/meo_bj_new.npy').item()
uv_bj = np.load('npy/uv_bj_new.npy').item()

datetimes = np.load('npy/datetimes_bj_new.npy')
aq_stationId = np.load('npy/aq_stationId.npy').item()


aq_pm25_ld = np.load('npy/aq_pm25_ld_new.npy').item()
aq_pm10_ld = np.load('npy/aq_pm10_ld_new.npy').item()
aq_o3_ld = np.load('npy/aq_o3_ld_new.npy').item()

meo_ld = np.load('npy/meo_ld_new.npy').item()
uv_ld = np.load('npy/uv_ld_new.npy').item()

datetimes_ld = np.load('npy/datetimes_ld_new.npy')

aq_stationId_ld = np.load('npy/aq_stationId_ld.npy').item()




import matplotlib.pyplot as plt
pm25s={}
pm10s={}
o3s={}

meos={}
uvs={}

for aq_id in aq_stationId:
    print('\nPreprocessing Beijing ...')
    print(aq_id, end=', ')
    pm25 = aq_pm25[aq_id]
    pm10 = aq_pm10[aq_id]
    o3 = aq_o3[aq_id]
    uv = uv_bj[aq_id]
    
    meo = []
    for i in range(13):
        arr = np.array(meo_bj[aq_id][i])
        meo.append(arr-arr%5)

    
    
    pm25 = pd.Series(pm25, index=datetimes)
    pm10 = pd.Series(pm10, index=datetimes)
    o3 = pd.Series(o3, index=datetimes)
    uv = pd.Series(uv, index=datetimes)
    
    for i in meo_range:
        meo[i] = pd.Series(meo[i], index=datetimes)

    
    pm25s[aq_id] = pm25
    pm10s[aq_id] = pm10
    o3s[aq_id] = o3
    uvs[aq_id] = uv
    
    meos[aq_id] = []
    for i in meo_range:
        meos[aq_id].append(meo[i])
    
for aq_id in aq_stationId_ld:
    print('\nPreprocessing London ...')
    print(aq_id, end=', ')
    pm25 = aq_pm25_ld[aq_id]
    pm10 = aq_pm10_ld[aq_id]
    uv = uv_ld[aq_id]
    
    meo = []
    for i in range(13):
        meo.append(meo_ld[aq_id][i])

    
    
    pm25 = pd.Series(pm25, index=datetimes_ld)
    pm10 = pd.Series(pm10, index=datetimes_ld)
    uv = pd.Series(uv, index=datetimes_ld)
    
    for i in meo_range:
        meo[i] = pd.Series(meo[i], index=datetimes_ld)
    #prec = pd.Series(prec, index=datetimes_ld)
    #ws = pd.Series(ws, index=datetimes_ld)
    
    
    
    pm25s[aq_id] = pm25
    pm10s[aq_id] = pm10
    uvs[aq_id] = uv
    
    #precs[aq_id] = prec
    #wss[aq_id] = ws
    meos[aq_id] = []
    for i in meo_range:
        meos[aq_id].append(meo[i])
    




X_trains={}
y_trains={}
X_tests={}
y_tests={}
    
for station in aq_stationId|aq_stationId_ld:
    print('\nPreparing training/testing data ...')
    print(station, end=', ')
    X_train = []
    y_train = []
    X_test = []
    y_test =[]  
    
    start_dt = datetime.datetime(2017,1,2,0,0,0)#'2017-04-15 00:00:00')
    
    end_dt = datetime.datetime(2018,m_end,d_end,0,0,0)
    
    
    split_dt = datetime.datetime(2018,m_start,d_start,0,0,0)
    
    train_len = (split_dt-start_dt).days*24
    test_len = (end_dt-split_dt).days*24
    
    pm25 = pm25s[station][start_dt:].values
    pm10 = pm10s[station][start_dt:].values
    uv = uvs[station][start_dt:].values

    meo = []
    for i in range(len(meos[station])):
        meo.append(meos[station][i][start_dt:].values)
    #prec = precs[station][start_dt:].values
    #ws = wss[station][start_dt:].values
    
    if station in o3s:
        o3 = o3s[station][start_dt:].values
        
    else:
        o3 = [0]*len(pm25)
    
    
    for i in range(0, train_len, 24):
        feature = np.hstack([pm25[i:i+24], pm10[i:i+24], o3[i:i+24]])
        for f in range(len(meo)):
            feature = np.hstack([feature, meo[f][i+24:i+72]])
        feature = np.hstack([feature, uv[12], uv[24+12], uv[48+12]])
        X_train.append(feature)
        
        if i+72>=len(pm25):
            #print(pm25[i+24:i+48].shape, np.zeros((24,)).shape, np.r_[pm25[i+24:i+48], np.zeros((24,))].shape)
            _z=np.zeros((48,))
            y_train.append([_z,_z,_z])
            print('train out of boundary')
            raise Exception
        else:
            y_train.append([pm25[i+24:i+72], pm10[i+24:i+72], o3[i+24:i+72]])
        
    for i in range(train_len, train_len + test_len, 24):
        feature = np.hstack([pm25[i:i+24], pm10[i:i+24], o3[i:i+24]])
        for f in range(len(meo)):
            feature = np.hstack([feature, meo[f][i+24:i+72]])
        feature = np.hstack([feature, uv[12], uv[24+12], uv[48+12]])
        X_test.append(feature)
        
        if i+72>=len(pm25):
            _z=np.zeros((48,))
            y_test.append([_z,_z,_z])
            print('test out of boundary')
            raise Exception
        else:
            y_test.append([pm25[i+24:i+72], pm10[i+24:i+72], o3[i+24:i+72]])
        
    xx,yy = np.array(X_train).shape
    for i in range(xx):
        for j in range(yy):
            if X_train[i][j] < 0:
                X_train[i][j] = np.float_power(10, X_train[i][j])

    X_trains[station]=np.array(X_train)
    y_trains[station]=np.array(y_train)
    X_tests[station]=np.array(X_test)
    y_tests[station]=np.array(y_test)
    

from datetime import timedelta, date, datetime


def daterange(datetime1, datetime2):
    for n in range(int ((datetime2 - datetime1).days + 1)*24):
        yield datetime1 + timedelta(days=n)


pred_dt=[]
for dt in daterange(split_dt, end_dt):
    s = dt.strftime("%Y-%m-%d")
    if s.startswith(end_dt.strftime('%Y-%m-%d')):
        break
    pred_dt.append(s)
    
import datetime

def SMAPE(F,A):
    l=[]
    for i in range(len(F)):
        de=[]
        nu=[]
        for j in range(len(F[i])):
            if abs(F[i][j])+abs(A[i][j])==0:
                de.append(0.)
                nu.append(1.)
            else:
                de.append(abs(F[i][j]-A[i][j]))
                nu.append(abs(F[i][j])+abs(A[i][j]))
        l.append(np.array(de)/(np.array(nu)))
    return 2*np.array(l).mean()

def simple_station(station):
    if not 'aq' in station:
        return station
    else:
        return station.split('_aq')[0][:10]+'_aq'

from sklearn.ensemble import RandomForestRegressor
import pickle

f = open(f_name, 'w')
if KDD:
    f.write('test_id,PM2.5,PM10,O3\n')
else:
    f.write('submit_date,test_id,PM2.5,PM10,O3\n')

train_smape=[]
test_smape=[]

aq_stationId_all=[]
for station in aq_stationId:
    aq_stationId_all.append(station)
for station in aq_stationId_ld:
    aq_stationId_all.append(station)

for station in aq_stationId_all:
    print('Training ...')
    print(station, end=', ')
    X_train=X_trains[station]
    y_train=y_trains[station]
    X_test=X_tests[station]
    y_test=y_tests[station]
    
    _a=[]
    _b=[]
    
    y_preds=[]
    y_train_preds=[]
    
    model_f_dir = 'model/{}_{}'.format(m_now, d_now)
    if not os.path.isdir(model_f_dir):
        os.makedirs(model_f_dir)

    for n in range(3):
        
        
        y_pred=[]
        y_train_pred=[]

        if station in aq_stationId:
            XX_train = np.c_[X_train[:,n*24:n*24+24],X_train[:,3*24:-1]]
            XX_test = np.c_[X_test[:,n*24:n*24+24],X_test[:,3*24:-1]]
        elif station in aq_stationId_ld:
            XX_train = X_train[:,n*24:n*24+24]
            XX_test = X_test[:,n*24:n*24+24]

        for h in range(48):
            
            model_f_name = os.path.join(model_f_dir, 'model_{}_n_{}_h_{}_model_09.sav'.format(station, n, h))

            if os.path.isfile(model_f_name):
                model = pickle.load(open(model_f_name, 'rb'))
            else:
                model = RandomForestRegressor(max_depth=depth, random_state=7198, n_estimators=n_estimators)

                model.fit(XX_train, y_train[:,n,h])
                pickle.dump(model, open(model_f_name, 'wb'))

            y_pred_h = model.predict(XX_test)
            y_train_pred_h = model.predict(XX_train)
            
            #regr[h].fit(X_train[:,n,:], y_train[:,n,h])
            #y_pred_h = regr[h].predict(X_test[:,n,:])
            #y_train_pred_h = regr[h].predict(X_train[:,n,:])
            

            y_pred.append(y_pred_h)
            y_train_pred.append(y_train_pred_h)
            
        y_pred=np.array(y_pred).T
        y_train_pred=np.array(y_train_pred).T
        
        y_preds.append(y_pred)
        y_train_preds.append(y_train_pred)
        
        
        _a.append(SMAPE(y_train_pred, y_train[:,n,:]))
        _b.append(SMAPE(y_pred, y_test[:,n,:]))
        
    train_smape.append(_a)
    test_smape.append(_b)
        


    xx,yy,zz = np.array(y_preds).shape
    for i in range(xx):
        for j in range(yy):
            for k in range(zz):
                if y_preds[i][j][k] < 0:
                    y_preds[i][j][k] = np.float_power(10, y_preds[i][j][k])
        

    #print(X_train.shape, y_train.shape, X_test.shape, np.array(y_preds).shape)
    
    for i, dt in enumerate(pred_dt):
        for n in range(48):
            #print('{},{}#{},{},{},{}'.format(dt,station,n,y_preds[0][i][n],y_preds[1][i][n],y_preds[2][i][n]))
            if KDD:
                f.write('{}#{},{},{},{}\n'.format(simple_station(station),n,y_preds[0][i][n],y_preds[1][i][n],y_preds[2][i][n]))
            else:
                f.write('{},{}#{},{},{},{}\n'.format(dt,simple_station(station),n,y_preds[0][i][n],y_preds[1][i][n],y_preds[2][i][n]))
                #print('{},{}#{},{},{},{}\n'.format(dt,simple_station(station),n,y_preds[0][i][n],y_preds[1][i][n],y_preds[2][i][n]))
f.close()



train_smape=np.array(train_smape)
test_smape=np.array(test_smape)

np.save('test_smape.npy', test_smape)

tot=0.
cnt=0.
for x in test_smape:
    for y in x:
        if y!=0:
            tot+=y
            cnt+=1
#print('total smape:')
#print(tot/cnt)

#print('seperate smape:')
#print(test_smape.mean(axis=0))

tot=0.
cnt=0.
for x in test_smape:
    for y in [x[2]]:
        if y!=0:
            tot+=y
            cnt+=1
#print('O3 smape:')
#print(tot/cnt)



