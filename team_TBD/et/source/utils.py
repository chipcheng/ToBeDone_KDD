import logging
import json
import yaml
import numpy as np
import pandas as pd



def get_logger():
    logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger

def print_config(config):
    print(json.dumps(config, indent=2))
    
def read_config(config_path):
    with open(config_path, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    print_config(cfg)
    return cfg


def symmetric_mean_absolute_percentage_error(actual, predicted):
    a = np.abs(np.array(actual) - np.array(predicted))
    b = np.array(actual) + np.array(predicted)
    
    return 2 * np.mean(np.divide(a, b, out=np.zeros_like(a), where=b!=0, casting='unsafe'))
    # mask = ~((y_true==0) & (y_pred==0))
    # y_true = y_true[mask]
    # y_pred = y_pred[mask]
    # return np.mean((np.abs(y_pred - y_true) * 200/ (np.abs(y_pred) + np.abs(y_true))))

def huber_approx_obj(preds, dtrain):
    d = dtrain.get_labels() - preds  #remove .get_labels() for sklearn
    h = 1  #h is delta in the graphic
    scale = 1 + (d / h) ** 2
    scale_sqrt = np.sqrt(scale)
    grad = d / scale_sqrt
    hess = 1 / scale / scale_sqrt
    return grad, hess

def build_submission_dataframe(df):

    logger = get_logger()
    logger.info("Build Submission")

    station_list = [
        'aotizhongxin_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq',
       'dingling_aq', 'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq',
       'fangshan_aq', 'fengtaihuayuan_aq', 'guanyuan_aq', 'gucheng_aq',
       'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq',
       'miyunshuiku_aq', 'nansanhuan_aq', 'nongzhanguan_aq',
       'pingchang_aq', 'pinggu_aq', 'qianmen_aq', 'shunyi_aq',
       'tiantan_aq', 'tongzhou_aq', 'wanliu_aq', 'wanshouxigong_aq',
       'xizhimenbei_aq', 'yanqin_aq', 'yizhuang_aq', 'yongdingmennei_aq',
       'yongledian_aq', 'yufa_aq', 'yungang_aq', 'zhiwuyuan_aq', 
       'CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4']


    df.columns = df.columns.set_names('hour')
    df = df.stack().unstack('pollutant')
    df = df.reset_index()
    is_forecast = df['station_id'].isin(station_list)
    df = df[is_forecast]
    df['hour'] = df['hour'].astype(str)
    df['station_id'] = df['station_id'].apply(lambda x: x.replace('_aq','')[:10] + '_aq' if 'aq' in x else x)
    df['test_id'] = df[['station_id', 'hour']].apply(lambda x: '#'.join(x), axis=1)
    df['submit_date'] = df['date'] - pd.offsets.Day(1)
    df = df.drop(['station_id','hour','city','date'], axis=1)
    if 'PM25' in df : df.rename(columns={'PM25':'PM2.5'}, inplace=True)

    # if OFFICIAL_FLAG:
        # df = df[['submit_date','test_id','PM2.5','PM10','O3']]
        # df = df.drop(['submit_date'], axis=1)

    df = df.set_index(['submit_date','test_id'])

    logger.info("Submission Dataframe shape : " + str(df.shape))

    return df

def write_submission(df, submission_path):

    logger = get_logger()
    logger.info("Wrtie Submission")
    logger.info("Submission path : {}".format(submission_path))

    df = df[['PM2.5','PM10','O3']]

    df.to_csv(submission_path)

