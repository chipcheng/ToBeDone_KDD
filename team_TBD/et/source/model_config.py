import argparse
import yaml
import h5py
import json
import os

from utils import print_config

class ModelConfig():

    def __init__(self, config_path, save_flag=False, args=None):

        self.config_path = config_path
        if args:
            self.cfg = self.build_config(args)
        else:
            self.cfg = self.read_config(config_path)

        print_config(self.cfg)

        if save_flag :
            self.save_config(self.cfg, config_path)


    def build_config(self, args):

        cfg = {}

        #### dataset ex. dataset001
        cfg['dataset_name'] = args.dataset_name

        #### submit ex. official0415 or internal1
        cfg['submit_type'] = args.submit_type

        #### task

        ## London - PM25
        if args.ld_PM25:
            if 'task' not in cfg: cfg['task'] = {}
            if 'ld' not in cfg['task']: cfg['task']['ld'] = {}
            cfg['task']['ld']['PM25'] = {}
            cfg['task']['ld']['PM25']['bundles_file'] = args.ld_PM25.replace(".yml", ".h5")
            cfg['task']['ld']['PM25']['station_split'] = args.ld_PM25_station_split
            cfg['task']['ld']['PM25']['hour_split'] = args.ld_PM25_hour_split

        ## London - PM10
        if args.ld_PM10:
            if 'task' not in cfg: cfg['task'] = {}
            if 'ld' not in cfg['task']: cfg['task']['ld'] = {}
            cfg['task']['ld']['PM10'] = {}
            cfg['task']['ld']['PM10']['bundles_file'] = args.ld_PM10.replace(".yml", ".h5")
            cfg['task']['ld']['PM10']['station_split'] = args.ld_PM10_station_split
            cfg['task']['ld']['PM10']['hour_split'] = args.ld_PM10_hour_split

        ## Beijing - PM25
        if args.bj_PM25:
            if 'task' not in cfg: cfg['task'] = {}
            if 'bj' not in cfg['task']: cfg['task']['bj'] = {}
            cfg['task']['bj']['PM25'] = {}
            cfg['task']['bj']['PM25']['bundles_file'] = args.bj_PM25.replace(".yml", ".h5")
            cfg['task']['bj']['PM25']['station_split'] = args.bj_PM25_station_split
            cfg['task']['bj']['PM25']['hour_split'] = args.bj_PM25_hour_split

        ## Beijing - PM10
        if args.bj_PM10:
            if 'task' not in cfg: cfg['task'] = {}
            if 'bj' not in cfg['task']: cfg['task']['bj'] = {}
            cfg['task']['bj']['PM10'] = {}
            cfg['task']['bj']['PM10']['bundles_file'] = args.bj_PM10.replace(".yml", ".h5")
            cfg['task']['bj']['PM10']['station_split'] = args.bj_PM10_station_split
            cfg['task']['bj']['PM10']['hour_split'] = args.bj_PM10_hour_split

        ## Beijing - O3
        if args.bj_O3:
            if 'task' not in cfg: cfg['task'] = {}
            if 'bj' not in cfg['task']: cfg['task']['bj'] = {}
            cfg['task']['bj']['O3'] = {}
            cfg['task']['bj']['O3']['bundles_file'] = args.bj_O3.replace(".yml", ".h5")
            cfg['task']['bj']['O3']['station_split'] = args.bj_O3_station_split
            cfg['task']['bj']['O3']['hour_split'] = args.bj_O3_hour_split

        print(json.dumps(cfg, indent=2))

        return cfg

    def save_config(self, cfg, config_path):

        with open(config_path, 'w') as ymlfile:
            yaml.dump(cfg, ymlfile, default_flow_style=False, indent=4)

    def read_config(self, config_path):
        
        with open(config_path, 'r') as ymlfile:
            cfg = yaml.load(ymlfile)
        print_config(cfg)
        return cfg
