#!/bin/bash

date=$1

month="$(cut -d'-' -f1 <<< "$date" )"
day="$(cut -d'-' -f2 <<< "$date" )"

genres=( "test" "valid" )

for genre in ${genres[@]}
do 

	flag="official$month$day-$genre" ## official0430-test
	submit_type=$flag
	dataset_name=$flag
	model_tag="31"
	echo "DATE : $date"
	echo "submit_type : $submit_type" ## official0430-test
	echo "dataset_name : $dataset_name" ## official0430-test
	echo "model_tag : $model_tag" ## 31

	python3 main.py --regressor et \
		--dataset_name $dataset_name \
		--submit_type $submit_type \
		--bundles_dir ../data/feature/$submit_type \
		--ld_PM25 ld_PM25_forecast.yml \
		--ld_PM10 ld_PM10_forecast.yml \
		--bj_PM25 bj_PM25_forecast.yml \
		--bj_PM10 bj_PM10_forecast.yml \
		--bj_O3 bj_O3_forecast.yml \
		--submit_path ../../../output/${genre}/${genre}_${date}_${model_tag}.csv \
		--submit 

done


