today=$1"-23"

(cd ./Model/;
sh run_DNN.sh 1 1 1 PM25 $today bj DNN 11;) &

(cd ./Model/;
sh run_DNN.sh 2 1 1 PM10 $today bj DNN 12;) &

(cd ./Model/;
sh run_DNN.sh 3 1 1 O3 $today bj DNN 11;) &

(cd ./Model/;
sh run_DNN.sh 4 1 1 PM25 $today ld DNN 11;) &

(cd ./Model/;
sh run_DNN.sh 5 1 1 PM10 $today ld DNN 9;) &
wait


cd ./Model/
python3 combine.py --end_time $today --submit_type test
python3 combine.py --end_time $today --submit_type ensemble

today_date_without_year=$2

#ToBeDone/team_TBD/wariard/source/Model/
cp "../../submission/test/"$today"_03.csv" "../../../../output/test/test_"$today_date_without_year"_03.csv"
cp "../../submission/ensemble/"$today"_03.csv" "../../../../output/valid/valid_"$today_date_without_year"_03.csv"
