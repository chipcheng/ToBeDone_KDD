#system
import sys
import logging
logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#data
import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
import random

#config
import argparse

#command line
from subprocess import call

#station_info
from info import *

#model
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.model_selection import ParameterGrid

import ipdb

#args
def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', default='/home/guest/wariard/KDD/main_challenge/data/crawl_data/')
    parser.add_argument('--output_dir', default='/home/guest/wariard/KDD/main_challenge/data/filled_data/')

    parser.add_argument('--end_time', default='2018-04-18-12')
    parser.add_argument('--threshold_time', default='2018-03-01 00:00:00')
    parser.add_argument('--valid', default='y')

    parser.add_argument('--duration', default=1, type=int)
    parser.add_argument('--k', default=1, type=int)
    parser.add_argument('--interpolation_method', default='linear', type=str)

    parser.add_argument('--city', default='bj')
    parser.add_argument('--aq', default='aq')
    args = parser.parse_args()
    return args

def ffill_prev_ih_filter(data, x, info_dic):
    panda_list = []
    for station in info_dic["station_list"]:
        station_panda = data[data[info_dic["station_column_name"]] == station].copy()
        for label in info_dic["label_list"]:
            null_array = pd.isnull(station_panda[label]).values
            new_data = station_panda[label].values
            for i in range(x):
                if null_array[i]:
                    offset = i
                    while null_array[offset]:
                        offset += x
                        if offset >= len(null_array):
                            new_data[i] = 0.0
                            break
                    if offset < len(null_array):
                        new_data[i] = new_data[offset]
            for i in range(x, len(null_array)):
                if null_array[i]:
                    new_data[i] = new_data[i-x]
            station_panda[label] = new_data
        panda_list.append(station_panda)
    return pd.concat(panda_list)

def ffill_k_nearest_neighber_filter(data, k, info_dic):
    station_dic = info_dic["station_dic"]
    label_to_column = info_dic["label_to_column"]
    all_data = info_dic["all_data"]

    panda_list = []
    for station in info_dic["station_list"]:
        station_loc = np.asarray([float(station_dic[station]["longitude"]), float(station_dic[station]["latitude"])])
        distance_matrix = []
        for station_can in info_dic["station_list"]:
            station_can_loc = np.asarray([float(station_dic[station_can]["longitude"]), float(station_dic[station_can]["latitude"])])
            distance_matrix.append(np.linalg.norm(station_loc - station_can_loc))
        distance_zip = zip(info_dic["station_list"], distance_matrix)
        station_can_list, distance_can_list = zip(*sorted(distance_zip, key=lambda t: t[1]))
        same_section_list = station_can_list[1:1+k]

        station_panda = data[data[info_dic["station_column_name"]] == station].copy()
        for label in info_dic["label_list"]:
            null_array = pd.isnull(station_panda[label]).values
            new_data = station_panda[label].values
            column = label_to_column[label]
            for i in range(len(null_array)):
                if null_array[i]:
                    temp_data_list = []
                    for j in range(len(same_section_list)):
                        if not np.isnan(float(all_data[same_section_list[j]][i][column])):
                            temp_data_list.append(all_data[same_section_list[j]][i][column])
                    if len(temp_data_list) > 0:
                        new_data[i] = np.average(np.asarray(temp_data_list))
            station_panda[label] = new_data
        panda_list.append(station_panda)
    return pd.concat(panda_list)

def ffill_same_section_filter(data, info_dic):
    station_dic = info_dic["station_dic"]
    label_to_column = info_dic["label_to_column"]
    all_data = info_dic["all_data"]

    panda_list = []
    for station in station_list:
        station_panda = data[data[info_dic["station_column_name"]] == station].copy()
        section = station_dic[station]["type"]
        if section == "other":
            panda_list.append(station_panda)
            continue
        same_section_list = station_dic[station_dic[station]["type"]].copy().remove(station)
        for label in info_dic["label_list"]:
            null_array = pd.isnull(station_panda[label]).values
            new_data = station_panda[label].values
            column = label_to_column[label]
            for i in range(len(null_array)):
                if null_array[i]:
                    temp_data_list = []
                    for j in range(len(same_section_list)):
                        if not np.isnan(float(all_data[same_section_list[j]][i][column])):
                            temp_data_list.append(all_data[same_section_list[j]][i][column])
                    if len(temp_data_list) > 0:
                        new_data[i] = np.average(np.asarray(temp_data_list))
            station_panda[label] = new_data
        panda_list.append(station_panda)
    return pd.concat(panda_list)

#method = 'linear', 'polynomial', 'spline', 'cubic', 'quadratic'
def interpolation_filter(data, method, info_dic):
    panda_list = []
    for station in info_dic["station_list"]:
        station_panda = data[data[info_dic["station_column_name"]]==station].copy()
        for label in info_dic["label_list"]:
            station_panda[label] = station_panda[label].interpolate(method=method)
        panda_list.append(station_panda)
    return pd.concat(panda_list)

def check_nan(data_panda):
    return np.sum(pd.isnull(data_panda).values)

def ld_fix_negative(data_panda, label_list):
    for label in label_list:
        data_panda[label] = np.where(data_panda[label] < -1, 0, data_panda[label])
        data_panda[label] = np.where(data_panda[label] < 0, 0.1, data_panda[label])
    return data_panda

def fill_aq_train_data(data_panda, info_dic):
    nan = [check_nan(data_panda)]
    logger.info("Start Nans left: %d" %(nan[0]))

    data_panda = ffill_k_nearest_neighber_filter(data_panda, args.k, info_dic)
    nan.append(check_nan(data_panda))
    logger.info("After KNN filter, Nans left: %d, Reduces: %d" %(nan[1], nan[0]-nan[1]))

    data_panda = interpolation_filter(data_panda, args.interpolation_method, info_dic)
    nan.append(check_nan(data_panda))
    logger.info("After Interpolation filter, Nans left: %d, Reduces: %d" %(nan[2], nan[1]-nan[2]))

    data_panda = ffill_prev_ih_filter(data_panda, 24, info_dic)
    nan.append(check_nan(data_panda))
    logger.info("After FFill 24h filter, Nans left: %d, Reduces: %d" %(nan[3], nan[2]-nan[3]))
    logger.info("")

    if "NOx" in info_dic["label_list"]:
        logger.info("check negative! ")
        data_panda = ld_fix_negative(data_panda, info_dic["label_list"])

    return data_panda

def fill_aq_valid_data(data_panda, info_dic):
    nan = [check_nan(data_panda)]
    logger.info("Start Nans: %d" %(nan[0]))

    data_panda = ffill_k_nearest_neighber_filter(data_panda, args.k, info_dic)
    nan.append(check_nan(data_panda))
    logger.info("After KNN filter, Nans: %d, Reduces: %d" %(nan[1], nan[0]-nan[1]))

    data_panda = ffill_prev_ih_filter(data_panda, 24, info_dic)
    nan.append(check_nan(data_panda))
    logger.info("After FFill 24h filter, Nans: %d, Reduces: %d" %(nan[2], nan[1]-nan[2]))
    logger.info("")

    if "NOx" in info_dic["label_list"]:
        logger.info("check negative! ")
        data_panda = ld_fix_negative(data_panda, info_dic["label_list"])
        
    return data_panda

def fill_meo_data(data_panda, info_dic):
    nan = [check_nan(data_panda)]
    logger.info("Start Nans left: %d" %(nan[0]))
    if nan[0] == 0:
        logger.info("Clean Data!")
        return data_panda
    data_panda = ffill_prev_ih_filter(data_panda, 24, info_dic)
    nan.append(check_nan(data_panda))
    logger.info("After FFill 24h filter, Nans left: %d, Reduces: %d" %(nan[1], nan[0]-nan[1]))
    logger.info("")
    return data_panda

def prepare_info_dic(args, city, aq, data_panda, station_column_name):
    info_dic = {}
    if aq == 'aq':
        if city == 'bj':
            info_dic["station_dic"] = bj_aq_station_dic
            info_dic["label_to_column"] = bj_aq_column_dir
            info_dic["label_list"] = ['PM2.5', 'PM10', 'O3', 'NO2', 'SO2', 'CO']
        else:
            info_dic["station_dic"] = ld_aq_station_dic
            info_dic["label_to_column"] = ld_aq_column_dir
            info_dic["label_list"] = ['PM2.5', 'PM10', 'NO2', 'NO', 'NOx', 'SO2', 'CO', 'O3']

        all_data = {}
        station_list = np.unique(data_panda[[station_column_name]].values)
        for station in station_list:
            all_data[station] = data_panda[data_panda[station_column_name] == station].values

        info_dic["all_data"] = all_data
        info_dic["station_list"] = station_list
        info_dic["station_column_name"] = station_column_name
    else:
        info_dic["label_list"] = ['temperature', 'pressure', 'humidity', 'wind_direction', 'wind_speed/kph']
        info_dic["station_list"] = np.unique(np.unique(data_panda[[station_column_name]].values))
        info_dic["station_column_name"] = station_column_name

    return info_dic

def ld_data_filter(data_panda):
    logger.info("Filter outside data!")
    london_stat_list = ['BL0', 'CD9', 'CD1', 'GN0', 'GR4', 'GN3', 'GR9', 'HV1', 'KF1', 'LW2', 'ST5', 'TH4', 'MY7']
    data_panda = data_panda[data_panda["station_id"].isin(london_stat_list)]
    return data_panda

def processing(args, city, aq, station_column_name, time_column_name):
    threshold_time = datetime.strptime(args.threshold_time, "%Y-%m-%d %H:%M:%S")
    threshold_time_str = args.threshold_time.replace(" ", "-").split(":")[0]

    logger.info("Read_"+city+"_"+aq+"_"+args.end_time+".csv")
    data_panda = pd.read_csv(args.data_dir+str(city)+"_"+str(aq)+"_"+args.end_time+".csv")
    data_panda[time_column_name] = pd.to_datetime(data_panda[time_column_name])

    if city == "ld" and aq == "aq":
        data_panda = ld_data_filter(data_panda)

    if aq == "aq" and args.valid == "y":
        data_train_panda = data_panda[data_panda[time_column_name] <= threshold_time].copy()
        data_valid_panda = data_panda[data_panda[time_column_name] > threshold_time].copy()

        logger.info("Prepare_info_dic")
        train_info_dic = prepare_info_dic(args, city, aq, data_train_panda, station_column_name)
        logger.info("Train_info_dic finish")
        valid_info_dic = prepare_info_dic(args, city, aq, data_valid_panda, station_column_name)
        logger.info("Valid_info_dic finish")

        logger.info("")
        logger.info("Start Fill_" + city + "_" + aq + "_train_data")
        data_train_panda = fill_aq_train_data(data_train_panda, train_info_dic)
        logger.info("")
        logger.info("Start Fill_" + city + "_" + aq + "_valid_data")
        data_valid_panda = fill_aq_valid_data(data_valid_panda, valid_info_dic)
        data_panda = pd.concat([data_train_panda, data_valid_panda])
        if city == "ld":
            data_panda = ld_data_filter(data_panda)

    elif aq == "aq":
        logger.info("Prepare_info_dic")
        info_dic = prepare_info_dic(args, city, aq, data_panda, station_column_name)
        logger.info("")
        logger.info("Start Fill_" + city + "_" + aq + "_all_data")
        data_panda = fill_aq_train_data(data_panda, info_dic)
        logger.info("")
    else:
        data_train_panda = data_panda[data_panda[time_column_name] <= threshold_time].copy()
        data_valid_panda = data_panda[data_panda[time_column_name] > threshold_time].copy()

        logger.info("Prepare_info_dic")
        train_info_dic = prepare_info_dic(args, city, aq, data_train_panda, station_column_name)
        logger.info("Train_info_dic finish")
        valid_info_dic = prepare_info_dic(args, city, aq, data_valid_panda, station_column_name)
        logger.info("Valid_info_dic finish")

        logger.info("")
        logger.info("Start Fill_" + city + "_" + aq + "_train_data")
        data_train_panda = fill_meo_data(data_train_panda, train_info_dic)
        logger.info("")
        logger.info("Start Fill_" + city + "_" + aq + "_valid_data")
        data_valid_panda = fill_meo_data(data_valid_panda, valid_info_dic)
        data_panda = pd.concat([data_train_panda, data_valid_panda])


    if city == "bj" and aq == "aq":
        total_list = ['stationId','utc_time', 'PM2.5', 'PM10', 'O3', 'NO2', 'SO2', 'CO']
    elif city == "ld" and aq == "aq":
        total_list = ['station_id', 'MeasurementDateGMT', 'PM2.5', 'PM10', 'NO2', 'NO', 'NOx', 'SO2', 'CO', 'O3']
    else:
        total_list = ['stationName', 'longitude', 'latitude', 'utc_time', 'temperature', 'pressure', 'humidity', 'wind_direction', 'wind_speed/kph']

    logger.info(city+"_"+aq+" To csv")
    if args.valid == "y":
        data_panda.to_csv(args.output_dir+args.end_time+"/valid/"+city+"_"+aq+"_"+args.end_time+"_valid.csv",
                           index=False, columns=total_list)
    else:
        data_panda.to_csv(args.output_dir+args.end_time+"/test/"+city+"_"+aq+"_"+args.end_time+".csv",
                           index=False, columns=total_list)

if __name__ == '__main__':
    args = arg_parse()
    logger.info("Start")

    logger.info("mkdir end_time: " + str(args.end_time))
    call(["mkdir", args.output_dir + args.end_time])
    call(["mkdir", args.output_dir + args.end_time+"/valid/"])
    call(["mkdir", args.output_dir + args.end_time+"/test/"])

    if args.city == 'bj' and args.aq == 'aq':
        processing(args, "bj", "aq", "stationId", "utc_time")
    elif args.city == 'bj' and args.aq == 'meo':
        processing(args, "bj", "meo_grid", "stationName", "utc_time")
    elif args.city == 'ld' and args.aq == 'aq':
        processing(args, "ld", "aq", "station_id", "MeasurementDateGMT")
    elif args.city == 'ld' and args.aq == 'meo':
        processing(args, "ld", "meo_grid", "stationName", "utc_time")

