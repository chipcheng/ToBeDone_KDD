import pandas as pd
import numpy as np
import sys
from datetime import datetime
from datetime import timedelta
import logging
logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
import ipdb

def append_time_index(data, station_column_name, time_column_name, start_time, end_time):
    end_time_datetime = datetime.strptime(end_time, "%Y-%m-%d-%H") + timedelta(days=3)
    index_end_str = end_time_datetime.strftime("%Y-%m-%d-%H")
    date_index = pd.date_range(start_time, index_end_str, freq='H')
    data.loc[:, time_column_name] = pd.to_datetime(data[time_column_name])
    data = data.set_index([station_column_name, time_column_name])
    data = data[~data.index.duplicated(keep='first')]
    logger.info("unstack reindex stack swaplevel sort_index")
    data = data.unstack(level=0).reindex(date_index).stack(station_column_name, dropna=False).swaplevel(0,1).sort_index()
    logger.info("ffill")
    if station_column_name == 'stationName':
        data[['latitude', 'longitude']] = data[['latitude', 'longitude']].ffill()
    logger.info("station get_level_values")
    data[station_column_name] = data.index.get_level_values(station_column_name)
    logger.info("utc_time get_level_values")
    data[time_column_name] = data.index.get_level_values(level=1)
    return data

station_dictionary = {
    "1003A": "dongsi_aq",
    "1004A": "tiantan_aq",
    "1006A": "guanyuan_aq",
    "1001A": "wanshouxigong_aq",
    "1011A": "aotizhongxin_aq",
    "1005A": "nongzhanguan_aq",
    "1007A": "wanliu_aq",
    "1012A": "gucheng_aq",
    "1008A": "shunyi_aq",
    "1010A": "pingchang_aq",
    "1009A": "huairou_aq",
    "1002A": "dingling_aq"
}


raw_data = pd.read_csv(sys.argv[1])
new_data = pd.read_csv(sys.argv[2])
station_panda_list = []
label_list = ["PM2.5", "PM10", "O3", "NO2", "SO2", "CO"]

for _, station_key in enumerate(station_dictionary):
    raw_station_panda = new_data[["date", "hour", "type", station_key]]
    new_raw_station_panda = raw_station_panda[raw_station_panda["type"] == "PM2.5"]
    for label in label_list:
        raw_station_data = raw_station_panda[raw_station_panda["type"] == label][["date", "hour", "type", str(station_key)]].values
        utc_time_values = []
        label_values = []
        for i in range(len(raw_station_data)):
            utc_time_values.append(datetime.strptime(str(raw_station_data[i][0]), "%Y%m%d") + timedelta(hours=int(raw_station_data[i][1])) - timedelta(hours=8))
            label_values.append(raw_station_data[i][-1])
            
        if label == "PM2.5":
            new_raw_station_panda.loc[:, "stationId"] = station_dictionary[station_key]
            new_raw_station_panda.loc[:, "utc_time"] = utc_time_values
        new_raw_station_panda.loc[:, label] = label_values
    del new_raw_station_panda["date"]
    del new_raw_station_panda["hour"]
    del new_raw_station_panda["type"]
    del new_raw_station_panda[station_key]
    station_panda_list.append(new_raw_station_panda)

end_time_str = sys.argv[3]
external_data = pd.concat(station_panda_list)
backup_new_data = append_time_index(external_data, "stationId", "utc_time", "2017-01-01-14", end_time_str)
raw_data = append_time_index(raw_data, "stationId", "utc_time", "2017-01-01-14", end_time_str)
raw_data.update(backup_new_data)
raw_data = raw_data.reset_index(drop=True)
raw_data.to_csv(sys.argv[4], index=False, columns=["stationId", "utc_time", "PM2.5", "PM10", "O3", "NO2", "SO2", "CO"])