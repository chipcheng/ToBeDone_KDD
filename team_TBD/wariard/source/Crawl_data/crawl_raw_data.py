#data
import pandas as pd
import requests
import numpy as np
from datetime import timedelta
from datetime import datetime

#config
import argparse

#command line
from subprocess import call

#parallel
import multiprocessing
from joblib import Parallel, delayed

#system
import logging
logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
import ipdb

#args
def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', default='/home/guest/wariard/KDD/main_challenge/data/cronjob_raw_data/')
    parser.add_argument('--bj_aq', default='bj_aq.csv')
    parser.add_argument('--bj_meo_grid', default='bj_meo_grid.csv')
    parser.add_argument('--ld_aq', default='ld_aq.csv')
    parser.add_argument('--ld_meo_gri', default='ld_meo_grid.csv')
    parser.add_argument('--output_dir', default='/home/guest/wariard/KDD/main_challenge/data/crawl_data/')
    parser.add_argument('--tmp_file', default='/home/guest/wariard/KDD/main_challenge/data/cronjob_raw_data/tmp.csv')
    parser.add_argument('--start_time', default='2017-01-01 14:00:00')
    parser.add_argument('--end_time', default='2018-04-18-12')
    parser.add_argument('--city', default='bj')
    parser.add_argument('--aq', default='aq')
    args = parser.parse_args()
    return args

def station_panda_parallel(station_panda, station, station_column_name, time_column_name, date_index):
    station_panda = station_panda.set_index(time_column_name)
    station_panda.index = pd.to_datetime(station_panda.index)
    station_panda = station_panda.loc[date_index]
    station_panda[station_column_name] = station
    if station_column_name == 'stationName':
        station_panda[['latitude', 'longitude']] = station_panda[['latitude', 'longitude']].ffill()
    return station_panda

def append_time_index(data, station_column_name, time_column_name, start_time, end_time):
    end_time_datetime = datetime.strptime(end_time, "%Y-%m-%d-%H") + timedelta(days=3)
    index_end_str = end_time_datetime.strftime("%Y-%m-%d-%H")
    date_index = pd.date_range(start_time, index_end_str, freq='H')
    data[time_column_name] = pd.to_datetime(data[time_column_name])
    data = data.set_index([station_column_name, time_column_name])
    data = data[~data.index.duplicated(keep='first')]
    logger.info("unstack reindex stack swaplevel sort_index")
    data = data.unstack(level=0).reindex(date_index).stack(station_column_name, dropna=False).swaplevel(0,1).sort_index()
    logger.info("ffill")
    if station_column_name == 'stationName':
        data[['latitude', 'longitude']] = data[['latitude', 'longitude']].ffill()
    logger.info("station get_level_values")
    data[station_column_name] = data.index.get_level_values(station_column_name)
    logger.info("utc_time get_level_values")
    data[time_column_name] = data.index.get_level_values(level=1)
    return data

def get_backup_data(new_data, city, end_time_str, aq):
    logger.info("get_backup_data city: " + str(city) + ", aq: " + aq)
    if aq == 'air':
        backup_url = 'http://kdd.caiyunapp.com/competition/2k0d1d8/'+city+'/2018-04-27-16/'+end_time_str+'/'+aq
        backup_new_data = append_time_index(pd.read_csv(backup_url), "station_id", "time", "2018-04-27-16", end_time_str)
        new_data = append_time_index(new_data, "station_id", "time", "2018-04-27-16", end_time_str)
        backup_new_data.update(new_data)
        backup_new_data = backup_new_data.reset_index(drop=True)
    else:
        backup_url = 'http://kdd.caiyunapp.com/competition/2k0d1d8/'+city+'/2018-05-21-16/'+end_time_str+'/'+aq
        end_time_datetime = datetime.strptime(end_time_str, "%Y-%m-%d-%H") - timedelta(days=3)
        index_end_str = end_time_datetime.strftime("%Y-%m-%d-%H")
        backup_new_data = append_time_index(pd.read_csv(backup_url), "station_id", "time", "2018-05-21-16", index_end_str)
        new_data = append_time_index(new_data, "station_id", "time", "2018-04-27-16", index_end_str)
        new_data.update(backup_new_data)
        backup_new_data = new_data.reset_index(drop=True)
    return backup_new_data

def get_ld_external_data(new_data, end_time_str):
    station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4'] 
    station_panda_list = []
    end_time_datetime = datetime.strptime(end_time_str, "%Y-%m-%d-%H") + timedelta(days=1)
    daymonth = end_time_datetime.strftime("%d%b")
    for station in station_list:
        url = "http://api.erg.kcl.ac.uk/AirQuality/Data/Site/Wide/SiteCode="+station+"/StartDate=27Apr2018/EndDate="+daymonth+"2018/csv"
        try:
            station_panda = pd.read_csv(url)
            for key in station_panda.keys():
                if key.split(': ')[-1] == "Nitric Oxide (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "NO"})
                elif key.split(': ')[-1] == "Nitrogen Dioxide (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "NO2"})
                elif key.split(': ')[-1] == "Oxides of Nitrogen (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "NOx"})
                elif key.split(': ')[-1] == "PM10 Particulate (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "PM10"})
                elif key.split(': ')[-1] == "PM2.5 Particulate (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "PM2.5"})
                elif key.split(': ')[-1] == "Carbon Monoxide (mg/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "CO"})
                elif key.split(': ')[-1] == "Ozone (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "O3"})
                elif key.split(': ')[-1] == "Sulphur Dioxide (ug/m3)":
                    station_panda = station_panda.rename(index=str, columns={key: "SO2"})
            station_panda['station_id'] = station
            station_panda_list.append(station_panda)
        except:
            logger.info("Try to get station: %s failed!" %station)

    if len(station_panda_list) == 0:
        return new_data
    external_data = pd.concat(station_panda_list)

    new_data = append_time_index(new_data, "station_id", "MeasurementDateGMT", "2018-04-27-16", end_time_str)
    external_data = append_time_index(external_data, "station_id", "MeasurementDateGMT", "2018-04-27-16", end_time_str)
    external_data.update(new_data)
    external_data = external_data.reset_index(drop=True)
    return external_data

def crawl(args, end_time_str, city, aq, file_path):
    start_date = datetime(2018, 4, 27, 16)
    logger.info("HTTP request for " + str(city) +"_"+ str(aq))
    url = 'https://biendata.com/competition/'+aq+'/'+city+'/2018-04-27-16/'+end_time_str+'/2k0d1d8'
    new_data = pd.read_csv(url)
    logger.info("Finish read main api " + str(city) +"_"+ str(aq))
    #if city == "bj_grid" or aq == "airquality":
    #new_data = get_backup_data(new_data, city, end_time_str, aq)
    logger.info("Finish HTTP request!" + str(city) +"_"+ str(aq))
    if city == 'bj' and aq == 'airquality':
        ori_data = pd.read_csv(args.data_dir + args.bj_aq)
        ori_data['utc_time'] = pd.to_datetime(ori_data['utc_time'])
        ori_data = ori_data[ori_data['utc_time'] < start_date]
        new_data = new_data.rename(index=str, columns={"station_id": "stationId",
                                                       "time": "utc_time",
                                                       "PM25_Concentration": "PM2.5",
                                                       "PM10_Concentration": "PM10",
                                                       "O3_Concentration": "O3",
                                                       "NO2_Concentration": "NO2",
                                                       "SO2_Concentration": "SO2",
                                                       "CO_Concentration": "CO"})
        del new_data['id']
        data = pd.concat([ori_data, new_data]).drop_duplicates()
        data = append_time_index(data, "stationId", "utc_time", args.start_time, args.end_time)
        logger.info("to_csv")
        data.to_csv(file_path, index=False, columns=['stationId','utc_time', 'PM2.5', 'PM10', 'O3', 'NO2', 'SO2', 'CO'])

    elif city == 'bj_grid' and aq == 'meteorology':
        ori_data = pd.read_csv(args.data_dir + args.bj_meo_grid)
        ori_data['utc_time'] = pd.to_datetime(ori_data['utc_time'])
        ori_data = ori_data[ori_data['utc_time'] < start_date]
        new_data = new_data.rename(index=str, columns={"station_id": "stationName",
                                                       "time": "utc_time",
                                                       "wind_speed": "wind_speed/kph"})
        del new_data['id']
        del new_data['weather']
        forecast_data = pd.DataFrame([])
        while forecast_data.empty:
            forecast_url = "http://kdd.caiyunapp.com/competition/forecast/bj/"+end_time_str+"/2k0d1d8"
            forecast_data = pd.read_csv(forecast_url)
            end_time_str = (datetime.strptime(end_time_str, "%Y-%m-%d-%H")-timedelta(hours=1)).strftime("%Y-%m-%d-%H")
        logger.info("Final bj_meo_grid forecast utc_time: "+str(end_time_str))
        forecast_data = forecast_data.rename(index=str, columns={"station_id": "stationName",
                                                                 "forecast_time": "utc_time",
                                                                 "wind_speed": "wind_speed/kph"})
        del forecast_data['id']
        del forecast_data['weather']
        data = pd.concat([ori_data, new_data, forecast_data]).drop_duplicates()
        data = append_time_index(data, "stationName", "utc_time", args.start_time, args.end_time)
        logger.info("to_csv")
        data.to_csv(file_path, index=False, columns=['stationName', 
                                                     'longitude', 
                                                     'latitude', 
                                                     'utc_time', 
                                                     'temperature', 
                                                     'pressure', 
                                                     'humidity', 
                                                     'wind_direction', 
                                                     'wind_speed/kph'])

    elif city == 'ld' and aq == 'airquality':
        ori_data = pd.read_csv(args.data_dir + args.ld_aq)
        ori_data['MeasurementDateGMT'] = pd.to_datetime(ori_data['MeasurementDateGMT'])
        ori_data = ori_data[ori_data['MeasurementDateGMT'] < start_date]
        new_data = new_data.rename(index=str, columns={"time": "MeasurementDateGMT",
                                                       "PM25_Concentration": "PM2.5",
                                                       "PM10_Concentration": "PM10",
                                                       "NO2_Concentration": "NO2"})
        new_data = get_ld_external_data(new_data, end_time_str)
        data = pd.concat([ori_data, new_data]).drop_duplicates()
        data = append_time_index(data, "station_id", "MeasurementDateGMT", args.start_time, args.end_time)
        logger.info("to_csv")
        data.to_csv(file_path, index=False, columns=['station_id',
                                                     'MeasurementDateGMT',
                                                     'PM2.5',
                                                     'PM10',
                                                     'NO2',
                                                     'NO',
                                                     'NOx',
                                                     'SO2',
                                                     'CO',
                                                     'O3'])

    elif city == 'ld_grid' and aq == 'meteorology':
        ori_data = pd.read_csv(args.data_dir + args.ld_meo_gri)
        ori_data['utc_time'] = pd.to_datetime(ori_data['utc_time'])
        ori_data = ori_data[ori_data['utc_time'] < start_date]
        new_data = new_data.rename(index=str, columns={"station_id": "stationName",
                                                       "time": "utc_time",
                                                       "wind_speed": "wind_speed/kph"})
        del new_data['id']
        del new_data['weather']
        forecast_data = pd.DataFrame([])
        while forecast_data.empty:
            forecast_url = "http://kdd.caiyunapp.com/competition/forecast/ld/"+end_time_str+"/2k0d1d8"
            forecast_data = pd.read_csv(forecast_url)
            end_time_str = (datetime.strptime(end_time_str, "%Y-%m-%d-%H")-timedelta(hours=1)).strftime("%Y-%m-%d-%H")
        logger.info("Final ld_meo_grid forecast utc_time: "+str(end_time_str))
        forecast_data = forecast_data.rename(index=str, columns={"station_id": "stationName",
                                                                 "forecast_time": "utc_time",
                                                                 "wind_speed": "wind_speed/kph"})
        del forecast_data['id']
        del forecast_data['weather']
        data = pd.concat([ori_data, new_data, forecast_data]).drop_duplicates()
        data = append_time_index(data, "stationName", "utc_time", args.start_time, args.end_time)
        logger.info("to_csv")
        data.to_csv(file_path, index=False, columns=['stationName', 
                                                     'longitude', 
                                                     'latitude', 
                                                     'utc_time', 
                                                     'temperature',
                                                     'pressure',
                                                     'humidity',
                                                     'wind_direction',
                                                     'wind_speed/kph'])

if __name__ == '__main__':
    args = arg_parse()
    end_time_str = args.end_time
    if args.city == 'bj' and args.aq == 'aq':
        logger.info("Start Crawling bj_aq")
        crawl(args, end_time_str, "bj", "airquality", args.output_dir+"bj_aq_"+end_time_str+".csv")
    elif args.city == 'bj' and args.aq == 'meo':
        logger.info("Start Crawling bj_grid")
        crawl(args, end_time_str, "bj_grid", "meteorology", args.output_dir+"bj_meo_grid_"+end_time_str+".csv")
    elif args.city == 'ld' and args.aq == 'aq':
        logger.info("Start Crawling ld_aq")
        crawl(args, end_time_str, "ld", "airquality", args.output_dir+"ld_aq_"+end_time_str+".csv")
    elif args.city == 'ld' and args.aq == 'meo':
        logger.info("Start Crawling ld_grid")
        crawl(args, end_time_str, "ld_grid", "meteorology", args.output_dir+"ld_meo_grid_"+end_time_str+".csv")
