import pandas as pd
import numpy as np
import sys

a = pd.read_csv(sys.argv[1])

for label in ["PM2.5", "PM10", "O3"]:
    a[label] = np.where(a[label] == 0, np.nan, a[label])
    a[label] = a[label].interpolate(method='linear')
    a[label] = a[label].fillna(method='bfill')

a.to_csv(sys.argv[2], index=False, columns=["submit_date", "test_id", "PM2.5", "PM10", "O3"])
