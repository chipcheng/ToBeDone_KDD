for i in $@
do
    echo $i
    if !(echo $i | grep -Eq "05-"); then
        mv "/tmp2/TBD/test/test_"$1"_"$i.csv ./
        python3 ensemble.py -d $1 -m uniform
        python3 ensemble.py -d $1 -m linear
        mv ./test* /tmp2/TBD/test/
    fi
done