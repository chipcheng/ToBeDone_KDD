import requests
import sys
import pandas as pd

data_panda = pd.read_csv(sys.argv[1])
data_panda.to_csv(sys.argv[1], index=False, columns=['test_id', 'PM2.5', 'PM10', 'O3'])

files={'files': open(sys.argv[1],'rb')}
 
filename = sys.argv[1].split('/')[-1]
data = {
    "user_id": "kevinorjohn",   #user_id is your username which can be found on the top-right corner on our website when you logged in.
    "team_token": "a3159ee92a5771ae7ee4d723aa0f2b479a05771d92f2aed37902fb43588605b1", #your team_token.
    "description": filename,  #no more than 40 chars.
    "filename": filename, #your filename
}
 
url = 'https://biendata.com/competition/kdd_2018_submit/'
 
response = requests.post(url, files=files, data=data)
 
print(response.text)
