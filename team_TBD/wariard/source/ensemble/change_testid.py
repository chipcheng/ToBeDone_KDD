import pandas as pd
import sys

submission = pd.read_csv(sys.argv[1])
submission['hour'] = [int(test_id.split('#')[1]) for test_id in submission['test_id']]
submission['station_id'] = [test_id.split('#')[0] for test_id in submission['test_id']]
submission['station_id'] = submission['station_id'].apply(lambda x:x.replace('_aq', '')[:10] + "_aq" if 'aq' in x else x)
submission.loc[:, "test_id"] = submission['station_id'].str.cat(submission['hour'].apply(str), sep='#')
submission.to_csv(sys.argv[2], index=False, columns=["submit_date", "test_id", "PM2.5", "PM10", "O3"])