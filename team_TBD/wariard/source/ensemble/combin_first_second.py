import pandas as pd
import numpy as np

import sys

first_panda = pd.read_csv(sys.argv[1])
second_panda = pd.read_csv(sys.argv[2])

first_panda['hour'] = [int(test_id.split('#')[1]) for test_id in first_panda['test_id']]
second_panda['hour'] = [int(test_id.split('#')[1]) for test_id in second_panda['test_id']]
first_panda['stationid'] = [test_id.split('#')[0] for test_id in first_panda['test_id']]
second_panda['stationid'] = [test_id.split('#')[0] for test_id in second_panda['test_id']]

first_panda = first_panda[first_panda['hour'] < 24]
second_panda = second_panda[second_panda['hour'] >= 24]

final_panda = pd.concat([first_panda, second_panda]).sort_values(by=['stationid', 'hour'])
final_panda.to_csv(sys.argv[3], index=False, columns=['test_id', 'PM2.5', 'PM10', 'O3'])