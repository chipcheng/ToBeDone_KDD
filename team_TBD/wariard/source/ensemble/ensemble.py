import argparse
import logging
import yaml
import os
import requests
import warnings
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import GradientBoostingRegressor

logger = logging.getLogger(__name__)

def smape(actual, predicted):
    a = np.abs(np.array(actual) - np.array(predicted))
    b = np.array(actual) + np.array(predicted)

    score = 2 * np.mean(
        np.divide(a, b, out=np.zeros_like(a), where=(b != 0), casting='unsafe')
    )
    return score


def load_prediction(config, date, mode):
    # read files
    df_dict = {}
    concat_filename = []
    for filename in os.listdir(config['{}_dir'.format(mode)]):
        if date in filename:
            print (filename)
            concat_filename.append(int(filename.split('_')[2].split('.')[0]))
            df = pd.read_csv(
                os.path.join(config['{}_dir'.format(mode)], filename),
                parse_dates=['submit_date']
            )
            tmp = df['test_id'].str.split('#', expand=True)
            tmp.columns = ['station_id', 'hour']
            df = pd.concat([df, tmp], axis=1)
            offset = pd.to_timedelta(pd.to_numeric(df['hour']), unit='h')
            df['time'] = df['submit_date'].add(offset)
            df.drop(['hour'], axis=1, inplace=True)
            filename = filename.replace("{}_".format(mode), "")
            df_dict[filename] = df
    return df_dict, concat_filename


def load_files(config, date):
    # load valid files
    valid_df_dict, _ = load_prediction(config, date, 'valid')
    # load test files
    test_df_dict, concat_filename = load_prediction(config, date, 'test')

    # read label files
    label_s_dict = {}

    # read bj station label
    filename = "bj_aq_{}.csv".format(date)
    df = pd.read_csv(
        os.path.join(config['label_dir'], date, filename),
        parse_dates=['utc_time']
    )
    df.rename(
        columns={'stationId': 'station_id', 'utc_time': 'time'},
        inplace=True
    )
    df['station_id'] = df['station_id'].apply(
        lambda x: x.replace('_aq', '')[:10] + '_aq' if 'aq' in x else x)
    df.set_index(['station_id', 'time'], inplace=True)
    label_s_dict['bj_PM2.5'] = df['PM2.5']
    label_s_dict['bj_PM10'] = df['PM10']
    label_s_dict['bj_O3'] = df['O3']

    filename = "ld_aq_{}.csv".format(date)
    df = pd.read_csv(
        os.path.join(config['label_dir'], date, filename),
        parse_dates=['MeasurementDateGMT']
    )
    df.rename(columns={'MeasurementDateGMT': 'time'}, inplace=True)
    df['station_id'] = df['station_id'].apply(
        lambda x: x.replace('_aq', '')[:10] + '_aq' if 'aq' in x else x)
    df.set_index(['station_id', 'time'], inplace=True)
    label_s_dict['ld_PM2.5'] = df['PM2.5']
    label_s_dict['ld_PM10'] = df['PM10']

    data = {
        'valid': valid_df_dict,
        'test': test_df_dict,
        'label': label_s_dict,
    }
    return data, concat_filename


def log_cosh_obj(preds, dtrain):
    x = dtrain - preds
    grad = np.tanh(x)
    hess = 1 / np.cosh(x)**2
    return grad, hess


def linear_ensemble(valid_df, test_df, label_s, city_stations, method):
    valid_station_id_s = valid_df.index.get_level_values('station_id')
    test_station_id_s = test_df.index.get_level_values('station_id')

    # validation
    mask = valid_station_id_s.isin(city_stations)
    city_valid_df = valid_df[mask]
    city_valid_df.reset_index(
        ['submit_date', 'test_id'], drop=True, inplace=True)
    X_valid = city_valid_df.values
    y_valid = label_s.loc[city_valid_df.index].values

    if X_valid.ndim == 1:
        X_valid = X_valid.reshape((-1, 1))

    isnan = np.isnan(X_valid).any(axis=1) | np.isnan(y_valid)
    X_valid = X_valid[~isnan]
    y_valid = y_valid[~isnan]

    # testing
    mask = test_station_id_s.isin(city_stations)
    city_test_df = test_df[mask]
    X_test = city_test_df.values

    if X_test.ndim == 1:
        X_test = X_test.reshape((-1, 1))

    feature_importance = None
    if method == 'linear':
        regressor = LinearRegression()
        regressor.fit(X_valid, y_valid)
        feature_importance = regressor.coef_
    test_prediction = regressor.predict(X_test)
    valid_prediction = regressor.predict(X_valid)
    error = smape(y_valid, valid_prediction)

    df = pd.DataFrame(test_prediction, index=city_test_df.index)
    return df, error, feature_importance


def ensemble(data, method): # NOQA
    ld_stations = [
        'BL0', 'CD1', 'CD9', 'GN0', 'GN3', 'GR4',
        'GR9', 'HV1', 'KF1', 'LW2', 'MY7', 'ST5', 'TH4'
    ]
    bj_stations = [
        'aotizhongx_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq',
        'dingling_aq', 'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq',
        'fangshan_aq', 'fengtaihua_aq', 'guanyuan_aq', 'gucheng_aq',
        'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq',
        'miyunshuik_aq', 'nansanhuan_aq', 'nongzhangu_aq', 'pingchang_aq',
        'pinggu_aq', 'qianmen_aq', 'shunyi_aq', 'tiantan_aq', 'tongzhou_aq',
        'wanliu_aq', 'wanshouxig_aq', 'xizhimenbe_aq', 'yanqin_aq',
        'yizhuang_aq', 'yongdingme_aq', 'yongledian_aq', 'yufa_aq',
        'yungang_aq', 'zhiwuyuan_aq'
    ]

    if method == 'uniform':
        df = None
        for key, value in data['test'].items():
            value = value.set_index(['station_id', 'time',
                                     'submit_date', 'test_id'])
            if df is None:
                df = value
            else:
                df = pd.concat([df, value], axis=1)
        result_df = df['PM2.5'].mean(axis=1)
        result_df = pd.concat([result_df, df['PM10'].mean(axis=1)], axis=1)
        result_df = pd.concat([result_df, df['O3'].mean(axis=1)], axis=1)
        result_df.columns = ['PM2.5', 'PM10', 'O3']
        result_df.sort_index(inplace=True)
        result_df.reset_index(['station_id', 'time'], drop=True, inplace=True)
        desc = "meth={}".format(method)
        return result_df, desc
    elif method in ['linear', 'gbr', 'xgb']:
        keys = set(data['test'].keys()) & set(data['valid'].keys())

        if len(data['test'].keys()) != len(data['test'].keys()):
            warnings.warn("Testing files and validation files are not equal")
        if not keys:
            raise Exception("Keys cannot not be empty")

        result_df = pd.DataFrame()
        scores = []
        for air in ['PM2.5', 'PM10', 'O3']:
            valid_df = None
            test_df = None
            for key in keys:
                # concat validation dataframe
                df = data['valid'][key].set_index(['station_id', 'time',
                                                   'submit_date', 'test_id'])
                if valid_df is None:
                    valid_df = df[air]
                else:
                    valid_df = pd.concat([valid_df, df[air]], axis=1)

                # concat testing dataframe
                df = data['test'][key].set_index(['station_id', 'time',
                                                  'submit_date', 'test_id'])
                if test_df is None:
                    test_df = df[air]
                else:
                    test_df = pd.concat([test_df, df[air]], axis=1)

            # for beijing
            label_s = data['label']['bj_{}'.format(air)]
            bj_df, score, importance = linear_ensemble(
                valid_df, test_df, label_s, bj_stations, method)
            scores.append(score)
            logger.info("bj_{} linear ensemble SAMPE {}".format(air, score))
            if importance is not None:
                for key, value in list(zip(list(keys), importance)):
                    logger.info("Weight {}: {:.6f}".format(key, value))
            if air != 'O3':
                # for london
                label_s = data['label']['ld_{}'.format(air)]
                ld_df, score, importance = linear_ensemble(
                    valid_df, test_df, label_s, ld_stations, method)
                logger.info("ld_{} linear ensemble: {}".format(air, score))
                scores.append(score)
                if importance is not None:
                    for key, value in list(zip(list(keys), importance)):
                        logger.info("Weight {}: {:.6f}".format(key, value))
            else:
                ld_df = pd.DataFrame()

            df = pd.concat([bj_df, ld_df])
            result_df = pd.concat([result_df, df], axis=1)
        logger.info("Average SAMPE score: {}".format(np.mean(scores)))
        result_df.columns = ['PM2.5', 'PM10', 'O3']
        result_df.sort_index(inplace=True)
        result_df.reset_index(['station_id', 'time'], drop=True, inplace=True)
        desc = "meth={},valid_score={:.5f}".format(method, np.mean(scores))
        return result_df, desc
    else:
        raise Exception("Method {} is not supported".format(method))


def get_args():
    methods = ['uniform', 'linear', 'gbr', 'xgb']
    parser = argparse.ArgumentParser("KDDCup2018 ensemble")
    parser.add_argument("--config",
                        default='./config.yml',
                        help="configuration")
    parser.add_argument("-d", "--date", help="submission date")
    parser.add_argument("-m", "--method", default=methods[0], choices=methods)
    return parser.parse_args()


def main(args):
    # read configuration
    with open(args.config) as fp:
        config = yaml.load(fp)

    logger.info("Loading files...")
    # read files
    data, concat_filename = load_files(config, args.date)

    logger.info("Running {} method...".format(args.method))
    # ensemble prediction results
    result_df, desc = ensemble(data, args.method)

    # clip negative value
    result_df[result_df < 0.] = 1.

    #time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
    concat_filename.sort()
    concat_filename = list(map(str, concat_filename))
    filenostring = "_".join(concat_filename)
    path = os.path.join(
        config['output_dir'],
        "ens_{}_{}_{}.csv".format(args.date, args.method, filenostring)
    )
    logger.info("Writing outputs to {}...".format(path))
    #result_df.reset_index(['submit_date'], drop=True, inplace=True)
    result_df.to_csv(path)


if __name__ == '__main__':
    args = get_args()
    logging.basicConfig(format='[%(asctime)s] %(message)s', level=logging.INFO)
    main(args)
