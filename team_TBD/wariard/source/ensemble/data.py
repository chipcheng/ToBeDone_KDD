import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta


class Data():
    def __init__(self, bj_data_path, ld_data_path, end_date):
        self.bj_data = pd.read_csv(bj_data_path)
        self.ld_data = pd.read_csv(ld_data_path)

        self.ld_station = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4']
        self.bj_station = np.unique(self.bj_data['stationId'].values)
        self.bj_station_dic = {station.split("_")[0][:10] + "_aq": station for station in self.bj_station}

        self.bj_data['utc_time'] = pd.to_datetime(self.bj_data['utc_time'])
        self.ld_data['MeasurementDateGMT'] = pd.to_datetime(self.ld_data['MeasurementDateGMT'])

        self.first_source = 

    def get_train_x_y(city, label):
        x = []
        y = []
        first_source = pd.read_csv("../data/valid_04-27_01.csv")
        second_source = pd.read_csv("../data/valid_04-27_02.csv")

        all_data_panda = pd.merge(first_source, second_source, on=['submit_date', 'test_id'])
        all_data_panda['submit_date'] = pd.to_datetime(all_data_panda['submit_date'])
        all_data = all_data_panda[['submit_date', 'test_id', label+"_x", label+"_y"]].values

        for data in all_data:
            place, offset = data[1].split('#')
            time = data[0] + timedelta(hours=int(offset))
            if city == 'bj':
                if place not in self.bj_station_short:
                    continue
                y_data = self.bj_data[(self.bj_data["stationId"] == self.bj_station_dic[place]) &
                                      (self.bj_data["utc_time"] == time)]
            else:
                if place not in self.ld_station:
                    continue
                y_data = self.ld_data[(self.ld_data["station_id"] == place) &
                                      (self.ld_data["MeasurementDateGMT"] == time)]['PM2.5'].values
            x.append([data[2][3]])
            y.append([y_data])
        return np.asarray(x), np.asarray(y)

    def get_test_x():
        pass
