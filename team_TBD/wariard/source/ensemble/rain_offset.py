import pandas as pd
import numpy as np

import sys

def clip(data_panda):
    for label in ['PM2.5', 'PM10', 'O3']:
        data_panda[label] = np.where(data_panda[label] < 0, 1.0, data_panda[label])
    return data_panda

def minus(data_panda, label, offset, hour_start):
    data_panda[label] = np.where(data_panda['hour'] >= hour_start, data_panda[label]-offset, data_panda[label])
    return data_panda

def multipul(data_panda, label, offset, hour_start, hour_end):
    data_panda[label] = np.where((data_panda['hour'] >= hour_start) & (), data_panda[label]*offset, data_panda[label])
    return data_panda


data_panda = pd.read_csv(sys.argv[1])

data_panda['hour'] = [int(test_id.split('#')[1]) for test_id in data_panda['test_id']]
data_panda['stationid'] = [test_id.split('#')[0] for test_id in data_panda['test_id']]


station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4'] 

beijing_panda = data_panda[~data_panda['stationid'].isin(station_list)]
london_panda = data_panda[data_panda['stationid'].isin(station_list)]

'''
for i in range(23, 28):
    beijing_panda = minus(beijing_panda, "PM2.5", -8, i)
    beijing_panda = minus(beijing_panda, "PM10", -10, i)

for i in range(32, 36):
    beijing_panda = minus(beijing_panda, "PM2.5", 14, i)
    beijing_panda = minus(beijing_panda, "PM10", 18, i)
'''
beijing_panda = minus(beijing_panda, "PM2.5", 5, 34)
beijing_panda = minus(beijing_panda, "PM2.5", 3, 35)
beijing_panda = minus(beijing_panda, "PM2.5", -3, 37)
beijing_panda = minus(beijing_panda, "PM2.5", -5, 38)

final_panda = pd.concat([beijing_panda, london_panda]).sort_values(by=['stationid', 'hour'])

final_panda = clip(final_panda)

final_panda.to_csv(sys.argv[2], index=False, columns=['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3'])
