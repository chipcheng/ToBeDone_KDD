export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
pyenv activate venv3.6.2

for i in $(seq 1 5)
do
    python3 ensemble.py -d "05-0"$i -m uniform
    python3 ensemble.py -d "05-0"$i -m linear
    sh eval.sh "05-0"$i
    rm "/home/guest/wariard/KDD/main_challenge/submission/all_ensemble/ens_05-0"$i"_"*
done

#for file_no in 02 04 06 07 13 26 31 
#do
#    mv "/home/guest/wariard/KDD/main_challenge/submission/all_test/test_05-0"*"_"$file_no".csv" "/home/guest/wariard/KDD/main_challenge/submission/all_temp/"
#    for i in $(seq 1 5)
#    do
#        python3 ensemble.py -d "05-0"$i -m uniform
#        python3 ensemble.py -d "05-0"$i -m linear
#        sh eval.sh "05-0"$i
#        rm "/home/guest/wariard/KDD/main_challenge/submission/all_ensemble/ens_05-0"$i"_"*
#    done
#    mv "/home/guest/wariard/KDD/main_challenge/submission/all_temp/"* "/home/guest/wariard/KDD/main_challenge/submission/all_test/"
#done

pyenv deactivate