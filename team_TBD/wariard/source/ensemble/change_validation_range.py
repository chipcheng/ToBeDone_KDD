import pandas as pd
from datetime import datetime
import sys

target_file = pd.read_csv("/tmp2/TBD/valid/valid_"+sys.argv[1]+"_03.csv")
target_file['submit_date'] = pd.to_datetime(target_file['submit_date'])

target_file = target_file[target_file['submit_date'] >= datetime.strptime(sys.argv[2], "%Y-%m-%d")]
target_file.to_csv("/tmp2/TBD/valid/valid_"+sys.argv[1]+"_03.csv", index=False, columns=['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3'])
