for i in $(seq 1 9)
do
    cp "./data/"$1"/bj_aq_"$1".csv" "./data/05-0"$i"/bj_aq_05-0"$i".csv" 
    cp "./data/"$1"/ld_aq_"$1".csv" "./data/05-0"$i"/ld_aq_05-0"$i".csv" 
done

for i in $(seq 10 $2)
do
    cp "./data/"$1"/bj_aq_"$1".csv" "./data/05-"$i"/bj_aq_05-"$i".csv" 
    cp "./data/"$1"/ld_aq_"$1".csv" "./data/05-"$i"/ld_aq_05-"$i".csv" 
done

cp "./data/"$1"/ld_aq_"$1".csv" "/tmp2/TBD/reproduce/ans/"