# for reproducibility
import random
random.seed(5314)
import numpy as np
np.random.seed(5314)

#system
import gc
import sys
import logging
logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#config
import argparse

#feature
import h5py

#data
import pandas as pd
from datetime import datetime
from datetime import timedelta
from sklearn.preprocessing import normalize

def get_session():
    gpu_options = tf.GPUOptions(allow_growth=True)
    return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

#args
def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--feature_path', default='../../feature/', type=str)
    parser.add_argument('--dataset_name', default='dataset_001')
    parser.add_argument('--city', default='bj')
    parser.add_argument('--label', default='PM25')
    parser.add_argument('--submission_path', default='../../submission/subset/')
    parser.add_argument('--model', default='DNN')
    parser.add_argument('--loss', default='mae')
    parser.add_argument('--epochs', default=7, type=int)
    parser.add_argument('--end_time', default='2018-05-01-23', type=str)
    parser.add_argument('--normalization', default='n')
    args = parser.parse_args()
    return args

def to_submission(args, model, feature, target_panda, is_valid, submit_type):
    if submit_type == "internal":
        submission_path = args.submission_path+args.end_time+"/"+args.dataset_name+"/"+args.city+"_"+args.label+"_internal.csv"
    elif submit_type == "ensemble":
        submission_path = args.submission_path+args.end_time+"/"+args.dataset_name+"/"+args.city+"_"+args.label+"_ensemble.csv"
    else:
        submission_path = args.submission_path+args.end_time+"/"+args.dataset_name+"/"+args.city+"_"+args.label+".csv"
    index = target_panda.index.values[is_valid]
    pred = model.predict(feature[is_valid])

    f = open(submission_path, "w")
    f.write("submit_date,test_id,"+args.label+"\n")
    for offset in range(len(index)):
        date = index[offset][1].to_pydatetime()-timedelta(days=1)
        if args.city == 'bj':
            place = index[offset][0].split('_')[0][:10] + "_aq"
        else:
            place = index[offset][0].split('_')[0][:10]
        for hour in range(len(pred[offset])):
            f.write(date.strftime("%Y-%m-%d")+","+place+"#"+str(hour)+","+str(pred[offset][hour])+"\n")
    f.close()


if __name__ == '__main__':
    args = arg_parse()
    if args.model == "DNN":
        from DNN_2 import DNN
        import tensorflow as tf
        tf.set_random_seed(5314)
        import keras.backend.tensorflow_backend as ktf
        ktf.set_session(get_session())

    all_data_panda = h5py.File(args.feature_path)
    all_feature = all_data_panda['features'].value
    target_panda = pd.read_hdf(args.feature_path, '/target/'+args.city+"_"+args.label+"_target_with_2018-06-30_s")
   
    all_feature = normalize(all_feature, norm='l2')

    is_train = all_data_panda['training_filters']
    
    #internal
    is_subvalid = all_data_panda['sub_validation_filters']
    
    #ensemble
    is_valid = all_data_panda['validation_filters']
    
    #test
    is_test = all_data_panda['true_testing_filters']

    score = 0
    best_epoch = 0

    if args.model == "DNN":
        DNN_model = DNN(all_feature, target_panda, is_train, is_subvalid, is_valid, args.loss, epochs=args.epochs)
        model, subvalid_score, valid_score, epoch = DNN_model.build_basic_model(DNN_model.training_data, DNN_model.subvalid_data, DNN_model.valid_data)
        to_submission(args, model, all_feature, target_panda, is_valid, "ensemble")
        data = [args.dataset_name, args.label, subvalid_score, valid_score, epoch, args.model]
        model, subvalid_score, valid_score, epoch = DNN_model.fine_tune(model, DNN_model.training_data, DNN_model.subvalid_data, DNN_model.valid_data) 

    to_submission(args, model, all_feature, target_panda, is_test, "test")
