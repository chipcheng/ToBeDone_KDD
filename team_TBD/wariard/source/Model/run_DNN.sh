#ToBeDone/team_TBD/wariard/source/Model/
for i in $(seq $2 $3)
do
    mkdir -p "../../submission/subset/"$5"/dataset_"$1"_"$i
    python3 model.py --feature_path "../../feature/"$5"/dataset_"$1"_"$i".h5" --dataset_name "dataset_"$1"_"$i --label $4 --city $6 --end_time $5 --model $7 --epochs $8
done