import pandas as pd
import numpy as np
import os
import tqdm

# AQ_ROOTDIR = '/nfs/Sif/runaway/kdd_data/Mao/up_to_date_data'
# META_ROOTDIR = '/nfs/Sif/runaway/kdd_data'

bj_station_meta = 'Beijing_AirQuality_Stations.csv'
ld_station_meta = 'London_AirQuality_Stations.csv'

ld_station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2',
                   'GN0', 'KF1', 'CD9', 'ST5', 'TH4']

bj_label_list = ['PM2.5', 'PM10', 'O3']
ld_label_list = ['PM2.5', 'PM10']


def interp_stations(data, limit=None):
    """ Interpolation for every station. """
    stat_groups = data.groupby('station')
    res = []
    for station in data.station.unique():
        ori = stat_groups.get_group(station)
        ori = ori.interpolate('linear', limit=limit)
        res.append(ori)
    return pd.concat(res, ignore_index=True)


def get_neighbors_names(meta, data):
    """ Return (center station) => [name of 1st closest, name of 2nd ...]"""
    res = {}

    for idx, row in meta.iterrows():
        center, lat, lon = row.station, row.Latitude, row.Longitude
        neighbors = {}
        for _, nrow in meta.iterrows():
            name, nlat, nlon = nrow.station, nrow.Latitude, nrow.Longitude
            neighbors[name] = np.square(nlon - lon) + np.square(nlat - lat)

        sorted_neighb = sorted(neighbors.items(), key=lambda x: x[1])
        res[center] = [neighbor[0] for neighbor in sorted_neighb[1:]]
    return res

def get_neighbors_idxs(meta, data):
    """ Return (center station) => [index of 1st closest, index of 2nd ...]"""
    res = {}
    st2idx = {s: idx for idx, s in enumerate(data.station.unique())}
    # print (st2idx)

    for idx, row in meta.iterrows():
        center, lat, lon = row.station, row.Latitude, row.Longitude
        neighbors = {}
        for _, nrow in meta.iterrows():
            name, nlat, nlon = nrow.station, nrow.Latitude, nrow.Longitude
            neighbors[name] = np.square(nlon - lon) + np.square(nlat - lat)

        sorted_neighb = sorted(neighbors.items(), key=lambda x: x[1])
        res[center] = [st2idx[neighbor[0]] for neighbor in sorted_neighb[1:]]
    return res


def get_bj_neighb(data, meta_rootdir, return_type='idx'):
    meta = pd.read_csv(os.path.join(meta_rootdir, bj_station_meta))
    meta.rename(columns={'station_name': 'station'}, inplace=True)
    if return_type == 'idx':
        return get_neighbors_idxs(meta, data)
    return get_neighbors_names(meta, data)


def get_ld_neighb(data, meta_rootdir, return_type='idx'):
    meta = pd.read_csv(os.path.join(meta_rootdir, ld_station_meta),
                       index_col=False)
    meta.rename(columns={'Unnamed: 0': 'station'}, inplace=True)
    meta = meta[meta['station'].isin(ld_station_list)]
    if return_type == 'idx':
        return get_neighbors_idxs(meta, data)
    return get_neighbors_names(meta, data)


def fill_knn(data, city, meta_rootdir, k=4):
    neighbors = get_bj_neighb(data, meta_rootdir) \
        if city == 'bj' else get_ld_neighb(data, meta_rootdir)
    columns = bj_label_list if city == 'bj' else ld_label_list

    time_group = data.groupby(['time'])
    for time, idxs in tqdm.tqdm(time_group.groups.items()):
        for r_idx in idxs:
            r = data.loc[r_idx]
            for gas in columns:
                if np.isnan(r[gas]):
                    sum, cnt = 0, 0
                    for stat_rnk in neighbors[r.station]:
                        val = data.loc[idxs[stat_rnk], gas]
                        if val != np.nan:
                            sum += val
                            cnt += 1
                        if cnt > k:
                            break
                    if cnt > 0:
                        data.loc[r_idx, gas] = sum / cnt
    return data


def add_missing_datetime(df, start=None, end=None):
    if start is None:
        hours = pd.DatetimeIndex(start=df['time'].min(),
                                 end=df['time'].max(), freq="1H")
    else:
        hours = pd.DatetimeIndex(start=start, end=end, freq="1H")

    station_groupby = df.groupby('station')
    res = []
    for station in station_groupby.groups:
        group = station_groupby.get_group(station)
        miss = hours[~hours.isin(group['time'])]
        new_df = pd.DataFrame()
        new_df['time'] = miss
        new_df['station'] = new_df['time'].apply(lambda x: station)
        group = group.append(new_df, ignore_index=True)
        group.sort_values('time', inplace=True)
        res.append(group)
    res = pd.concat(res, ignore_index=True)
    # print (res.head(1))
    return res


def read_file(aq_rootdir):
    bj = pd.read_csv(os.path.join(aq_rootdir, 'bj_aq.csv'),
                     parse_dates=['utc_time'])
    bj.rename(columns={'stationId': 'station', 'utc_time': 'time'},
              inplace=True)

    ld = pd.read_csv(os.path.join(aq_rootdir, 'ld_aq.csv'),
                     parse_dates=['MeasurementDateGMT'])
    ld.rename(columns={'MeasurementDateGMT': 'time', 'station_id': 'station',
                       'PM2.5 (ug/m3)': 'PM2.5',
                       'PM10 (ug/m3)': 'PM10',
                       'NO2 (ug/m3)': 'NO2'}, inplace=True)
    ld = ld[ld['station'].isin(ld_station_list)]

    bj = add_missing_datetime(bj)
    bj.sort_values(['station', 'time'], inplace=True)
    bj = bj.reset_index(drop=True)

    ld = add_missing_datetime(ld)
    ld.sort_values(['station', 'time'], inplace=True)
    ld = ld.reset_index(drop=True)

    """ station_id, time, PM2.5, PM10, (O3) """
    return (bj, ld)
