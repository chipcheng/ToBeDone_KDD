import preprocess as prep
import datetime as dt
import pandas as pd
from scipy.spatial import distance
from scipy.cluster.vq import whiten
import os
import numpy as np
import sys
import tqdm
import argparse

# AQ_ROOTDIR = '/nfs/Sif/runaway/kdd_data/Mao/up_to_date_data'
# META_ROOTDIR = '/nfs/Sif/runaway/kdd_data'
# OUTPUT_ROOTDIR = '/nfs/Sif/amy/median/'

NON_FEATURES = 'station,time,PM2.5,PM10,NO2,CO,O3,SO2'.split(',')

neighbors = None

def get_nearest_median(data, day):
    return np.median(data[-day:])


def get_nearest_mean(data, day):
    return np.mean(data[-day:])


def to_matrix(data, gas_col, day, get_nearest_func):
    res = np.zeros(24)
    hour_G = data.groupby('hour')
    for hour in range(24):
        group = hour_G.get_group(hour)
        res[hour] = get_nearest_func(group[gas_col].values, day)
    return res


def combine_ans(aq_data, ans, label_list, day, get_nearest_func, knn):
    ans_G = ans.groupby('station')
    station_G = aq_data.groupby('station')
    for station in station_G.groups:
        ans_stations = ans_G.get_group(station)
        stations = [station] + (neighbors[station][:knn] if neighbors else [])
        for gas in label_list:
            median = to_matrix(
                pd.concat([station_G.get_group(n) for n in stations],
                          ignore_index=True),
                gas, day, get_nearest_func)
            for idx, row in ans_stations.iterrows():
                ans.loc[idx, gas] = median[row.hour]
    return ans


def combine_group_ans(data, ans,
                      label_list, earlist_test, get_nearest_func, day):
    train = data[data.time < earlist_test]
    test = data[data.time >= earlist_test].copy(deep=True)

    test[label_list] = np.nan
    test = get_grouped_data(train, test, label_list, get_nearest_func, day)

    ans.loc[:, 'time'] = ans.apply(
        lambda r:
        pd.Timestamp(r['submit_date']) + pd.Timedelta(hours=r['offset']),
    axis=1)
    match = ['station', 'time']
    ans = pd.merge(ans[match + ['submit_date', 'test_id', 'offset']], test,
                   on=match, how='left', sort=False)
    return ans


def submission_df(filename, shortened_station=False, submit_date=None):
    ans = pd.read_csv(filename)
    if submit_date is not None:
        ans['submit_date'] = submit_date
    ans = ans[['test_id']]

    ans['station'] = ans['test_id'].apply(lambda x: x[:x.find('#')])
    if shortened_station:
        ans['station'] = ans['station'].apply(
            lambda x: (x.replace('_aq', '')[:10] + '_aq') if '_aq' in x else x)
    ans['offset'] = ans['test_id'].apply(lambda x: int(x[x.find('#') + 1:]))
    ans['hour'] = ans['offset'].apply(lambda x: x % 24)
    # Rebuild test_id to the maximum length of staion (10)
    ans['test_id'] = ans['test_id'].apply(lambda x: x.split('_aq'))
    ans['test_id'] = ans.apply(lambda r: "%s#%d" % (
        r['station'].replace('_aq', '')[:10] + '_aq' if '_aq' in r['station']
        else r['station'],
        r['offset']), axis=1)
    return ans


def get_grouped_data(train_df, test_df, target_cols, get_nearest_func, day):
    train_G = train_df.groupby('group')
    test_G = test_df.groupby('group')

    for n, group_idxs in test_G.groups.items():
        for gas_col in target_cols:
            test_df.loc[group_idxs, gas_col] = get_nearest_func(
                train_G.get_group(n)[gas_col].values, day)

    return test_df


def append_group(df, group_model):
    feature_cols = list(filter(lambda x: x not in NON_FEATURES, df.columns))
    print (feature_cols)
    codebook = np.load(group_model)
    whit_vec = whiten(df[feature_cols].values)
    print (whit_vec.shape, codebook.shape)
    dist = distance.cdist(whit_vec, codebook, 'euclidean')
    group = np.argmin(dist, axis=1)
    df['group'] = group


def prepare_group_data(aq_rootdir):
    earlist = pd.Timestamp('2018-01-01')
    bj_aq = pd.read_csv(os.path.join(aq_rootdir, 'bj_train_027.csv'),
                        parse_dates=['time'])
    bj_aq['station'] = bj_aq.station.apply(lambda x: x[:10] + '_aq')
    bj_aq = bj_aq[bj_aq.time >= earlist]

    ld_aq = pd.read_csv(os.path.join(aq_rootdir, 'ld_train_027.csv'),
                        parse_dates=['time'])
    ld_aq = ld_aq[ld_aq.time >= earlist]

    return bj_aq, ld_aq


def prepare_data(day_interval, aq_rootdir, meta_rootdir):
    earlist = pd.Timestamp('2018-05-01') - pd.Timedelta(days=day_interval + 3)

    bj_aq, ld_aq = prep.read_file(aq_rootdir)
    bj_aq = bj_aq[bj_aq.time >= earlist]
    ld_aq = ld_aq[ld_aq.time >= earlist]

    bj_aq = prep.fill_knn(bj_aq, 'bj', meta_rootdir, 4)
    bj_aq = prep.interp_stations(bj_aq)
    print ('Beijing nan:', bj_aq[prep.bj_label_list].isnull().values.sum())
    bj_aq['hour'] = bj_aq['time'].apply(lambda x: x.hour)

    ld_aq = prep.fill_knn(ld_aq, 'ld', meta_rootdir, 4)
    ld_aq = prep.interp_stations(ld_aq)
    print ('London nan:', ld_aq[prep.ld_label_list].isnull().values.sum())
    ld_aq['hour'] = ld_aq['time'].apply(lambda x: x.hour)

    return bj_aq, ld_aq


def main_for_group(args):
    METHODS = {
        'median': get_nearest_median,
        'mean': get_nearest_mean,
    }
    method = METHODS[args.method]
    INDEXES = {'median': 89, 'mean': 90}

    bj_aq, ld_aq = prepare_group_data(args.aq_rootdir)
    append_group(bj_aq, args.bj_kmeans_model)
    append_group(ld_aq, args.ld_kmeans_model)

    # Get submission dataframe
    sample = submission_df(os.path.join(args.meta_rootdir,
                                        'sample_submission.csv'),
                           shortened_station=True)

    month = 5
    today = pd.Timestamp.today().day
    target_range = tqdm.tqdm(range(1, 21)) if args.valid else \
        range(today - 1, today)
    # ranges = {3: range(31, 32), 4: range(1, 31), 5: range(20, 22)}

    results = []
    for date in target_range:
        answer = sample.copy(deep=True)
        answer['submit_date'] = "2018-0%d-%02d" % (month, date)
        bar = pd.Timestamp("2018-0%d-%02d 00:00:00" % (month, date))

        bj_ans = answer[answer.station.str.contains('_aq')]
        bj_ans = combine_group_ans(
            bj_aq, bj_ans, prep.bj_label_list, bar, method, args.day_interval)
        results.append(bj_ans[['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3']])

        ld_ans = answer[~answer.station.str.contains('_aq')]
        ld_ans = combine_group_ans(
            ld_aq, ld_ans, prep.ld_label_list, bar, method, args.day_interval)
        results.append(ld_ans[['submit_date', 'test_id', 'PM2.5', 'PM10']])

    results = pd.concat(results, ignore_index=True)
    outfile = 'test_0%d-%02d_%d.csv' % (month, date, INDEXES[args.method])
    if args.valid: outfile = 'valid_05.csv'
    results.to_csv(os.path.join(args.output_rootdir, outfile), index=False)

def main(args):
    METHODS = {
        'median': get_nearest_median,
        'mean': get_nearest_mean,
    }
    method = METHODS[args.method]

    INDEXES = {'median': 87, 'mean': 88}

    bj_aq, ld_aq = prepare_data(args.day_interval,
                                args.aq_rootdir, args.meta_rootdir)

    if args.knn > 0:
        global neighbors
        neighbors = prep.get_bj_neighb(bj_aq, 'name')
        neighbors.update(prep.get_ld_neighb(ld_aq, 'name'))

    # Get submission dataframe
    sample = submission_df(os.path.join(args.meta_rootdir,
                                        'sample_submission.csv'))

    end_date = dt.datetime.strptime(args.end_date, '%Y-%m-%d').date()
    start_date = dt.date(2018, 3, 31) if args.valid else end_date
    day_count = (end_date - start_date).days if args.valid else 1

    results = []
    for date in (start_date + dt.timedelta(n) for n in range(day_count)):
        answer = sample.copy(deep=True)
        answer['submit_date'] = "2018-%02d-%02d" % (date.month, date.day)
        bar = pd.Timestamp("2018-%0d-%02d 00:00:00" % (date.month, date.day))
        data = bj_aq[bj_aq.time < bar]
        answer = combine_ans(data, answer, prep.bj_label_list,
                             args.day_interval, method, args.knn)

        data = ld_aq[ld_aq.time < bar]
        answer = combine_ans(data, answer, prep.ld_label_list,
                             args.day_interval, method, args.knn)
        results.append(answer[['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3']])

    results = pd.concat(results, ignore_index=True)
    outfile = '%s_%02d-%02d_%d.csv' % (
        'valid' if args.valid else 'test',
        end_date.month, end_date.day, INDEXES[args.method])
    results.to_csv(os.path.join(args.output_rootdir, outfile), index=False)


def parse_args():
    parser = argparse.ArgumentParser(
        description='Handle history to provide better prediction.')
    # How to pick the group.
    parser.add_argument('-i', '--day-interval', type=int, default=30)
    parser.add_argument('-bjg', '--bj-kmeans-model', type=str)
    parser.add_argument('-ldg', '--ld-kmeans-model', type=str)

    # How to gather data.
    parser.add_argument('-m', '--method', type=str, default='median')
    parser.add_argument('-v', '--valid', action='store_true')
    parser.add_argument('-k', '--knn', type=int, default=0)

    # Root directories
    parser.add_argument('--end-date', type=str, required=True)
    parser.add_argument('-aq', '--aq-rootdir', required=True)
    parser.add_argument('-meta', '--meta-rootdir', required=True)
    parser.add_argument('-o', '--output-rootdir', required=True)
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    if args.bj_kmeans_model and args.ld_kmeans_model:
        main_for_group(args)
    else:
        main(args)
