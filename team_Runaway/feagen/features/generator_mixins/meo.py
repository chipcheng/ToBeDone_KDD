from feagen.decorators import (
    will_generate,
    require,
)
import numpy as np
import pandas as pd
from tqdm import tqdm

MEO_COLUMNS = ['pressure', 'wind_direction', 'wind_speed',
                'temperature', 'humidity']
WORLD_WEATHER_COLS = ['cloudcover', 'precipMM', 'visibility',
                      'pressure', 'tempC', 'windspeedKmph']
square = np.array([[1, 1], [1, -1], [-1, 1], [-1, -1]]) * .05


class GridGeneratorMixin(object):
    @require(['{city}_aq_df', '{city}_stations_df', '{city}_grid_df'])
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_stations_grid_df')
    def gen_city_stations_grid_df(self, data, will_generate_key, re_args):
        # Gets station meta.
        stat_meta = data['{city}_stations_df'].value
        locs = {row['station']: np.array([row['longitude'], row['latitude']])
                for i, row in stat_meta.iterrows()}

        grids = data['{city}_grid_df'].value.groupby(['longitude', 'latitude'])
        aq = data['{city}_aq_df'].value
        stat_groups = aq.groupby('station', sort=False)
        result_df = []
        # `unique` keeps the order of appearance!
        for stat in tqdm(aq['station'].unique()):
            dots = [tuple([round(x, 1) for x in (p + locs[stat])])
                    for p in square]
            trgs = [grids.get_group(l) for l in set(dots)]  # avoid duplicates
            neighbors = pd.concat(trgs, ignore_index=True)
            neighbors = neighbors.groupby(
                'time', as_index=False, sort=False).agg(
                {x: np.nanmean for x in MEO_COLUMNS})
            group = stat_groups.get_group(stat)[['station', 'time']]
            new = pd.merge(group, neighbors, on='time', how='left', sort=False)
            result_df.append(new)
        result_df = pd.concat(result_df, ignore_index=True)
        result_df.interpolate('linear', inplace=True)
        return result_df

    @require('{city}_stations_grid_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_grid_col_(?P<column>%s)'
                   % ('|'.join(MEO_COLUMNS)))
    def gen_grid_column(self, data, will_generate_key, re_args):
        df = data['{city}_stations_grid_df'].select(
            columns=[re_args['column']])
        return df

    @require(['bj_meo_df', 'bj_stations_grid_df'])
    @will_generate('pandas_hdf', r'bj_stations_comb_meo_grid_df')
    def gen_bj_comb_meo_grid_df(self, data, will_generate_key):
        """ Only Beijing contains both meo and grid data. """
        grid_df = data['bj_stations_grid_df'].value
        meo_df = data['bj_meo_df'].value
        # Workaround of data cannot have nan values => set to -1w.
        meo_df[meo_df[MEO_COLUMNS] < -1000] = np.nan
        KEY = ['station', 'time']
        merged = grid_df.merge(meo_df, on=KEY, how='left', suffixes=('', '_m'))
        meo_df = merged[list(map(lambda x: x + '_m', MEO_COLUMNS))]
        meo_df.rename(columns=lambda x: x[:-2], inplace=True)
        grid_df[MEO_COLUMNS] = meo_df.combine_first(merged[MEO_COLUMNS])
        return grid_df

    @require('bj_stations_comb_meo_grid_df')
    @will_generate('pandas_hdf', r'bj_comb_meo_grid_col_(?P<column>%s)'
                   % ('|'.join(MEO_COLUMNS)))
    def gen_bj_comb_meo_grid_col(self, data, will_generate_key, re_args):
        df = data['bj_stations_comb_meo_grid_df'].select(
            columns=[re_args['column']])
        return df

    @require('{city}_world_weather')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_station_(?P<col>%s)'
                   % ('|'.join(WORLD_WEATHER_COLS)))
    def gen_world_weather_col(self, data, will_generate_key, re_args):
        df = data['{city}_world_weather'].value
        ans = df[re_args['col']]
        print (ans.head())
        return ans
