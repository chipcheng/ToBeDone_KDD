import datetime
from feagen.decorators import (
    will_generate,
    require,
)


class FilterGeneratorMixin(object):
    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_internal_is_train')
    def gen_internal_is_train(self, data, will_generate_key, re_args):
        time = data['{city}_aq_df'].value['time']
        ans = time.dt.date < datetime.date(2018, 3, 1)
        return ans

    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_internal_is_test')
    def gen_internal_is_test(self, data, will_generate_key, re_args):
        time = data['{city}_aq_df'].value['time']
        ans = time.dt.date >= datetime.date(2018, 3, 1)
        return ans
