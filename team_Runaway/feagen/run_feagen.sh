#!/bin/bash
set -e # stop once any errors occur
umask 002

echo run_feagen.sh $1 $2...

action=$1	#train/test/both

dataset_id=$2
bundle_config_b_bj=".feagenrc/bundle_bj_config_"$dataset_id".yml"
bundle_config_b_ld=".feagenrc/bundle_ld_config_"$dataset_id".yml"

feagen_path="venv/bin/feagen"
feagen_python="venv/bin/python3"

bundle_config_g_predict=".feagenrc/config_predict.yml"
bundle_config_g_train=".feagenrc/config_train.yml"

#### Create virtual environment if not exists ####
if [ ! -d "venv" ]; then
  virtualenv -p python3 venv
  venv/bin/pip3 install -r requirements.txt
fi

############ Feagen executable cannot be used from other directories ########
echo "Check into ... ${PWD}"

if [[ $action == 'train' ]] || [[ $action == 'both' ]]; then
	$feagen_path -g $bundle_config_g_train -b $bundle_config_b_bj
	$feagen_python gen_feature.py train bj $dataset_id 

	$feagen_path -g $bundle_config_g_train -b $bundle_config_b_ld
	$feagen_python gen_feature.py train ld $dataset_id
  echo run_feagen train
fi

if [[ $action == 'test' ]] || [[ $action == 'both' ]]; then
	$feagen_path -g $bundle_config_g_predict -b $bundle_config_b_bj
	$feagen_python gen_feature.py predict bj $dataset_id

	$feagen_path -g $bundle_config_g_predict -b $bundle_config_b_ld
	$feagen_python gen_feature.py predict ld $dataset_id
  echo run_feagen train
fi

echo run_feagen.sh $1 $2 Done
