
# coding: utf-8

# In[17]:


import pandas as pd
import numpy as np
import argparse
from datetime import datetime, timedelta, date


# In[5]:

def get_internal_sample_sub_df(mao_path,val_end_date):
    val_end_date = datetime.strptime(val_end_date, '%Y-%m-%d')
    df = pd.read_csv(mao_path+'sam_sub.csv')
    df = df[df['submit_date']=='2018-02-28']
    val_start_date = datetime(2018, 3, 31)
    delta = val_end_date - val_start_date
    val_date = []
    for i in range(-(delta.days+3),-2):
        val_date.append((val_end_date + timedelta(hours=-8,days=i)).strftime('%Y-%m-%d'))
    ret_df = df.iloc[0:0]
    for d in val_date:
        df['submit_date'] = df['submit_date'].apply(lambda x:d)
        df['test_id'] = df['test_id'].apply(lambda x: x.split('_aq')[0][:10] + '_aq' + x.split('_aq')[1] if 'aq' in x else x)
        ret_df = ret_df.append(df,ignore_index=True)
    #ret_df['name'] = ret_df['test_id'].apply(lambda x:x.split('#')[0])
    #ret_df = ret_df.sort_values(by=['name','submit_date'],axis=0)
    ret_df = ret_df[['submit_date','test_id','PM2.5','PM10','O3']]
    return ret_df

def merge_internal(cur_path,ld_pred_csv_path,bj_pred_csv_path,output_path,sample_sub_path,end):
    ld_pred = pd.read_csv(ld_pred_csv_path)
    bj_pred = pd.read_csv(bj_pred_csv_path)
    merged = ld_pred.append(bj_pred)
    sample_sub = get_internal_sample_sub_df(cur_path,end)
    if(len(merged)!=len(sample_sub)):
        print('The length(days) of sample doesn\'t match.')
        return
    sample_sub['ind'] = pd.Series(np.arange(len(sample_sub)))
    sample_sub = sample_sub.sort_values(['test_id','submit_date'])
    merged = merged.sort_values(['test_id','submit_date'])
    merged = merged.reset_index(drop=True)
    sample_sub = sample_sub.reset_index(drop=True)
    merged['ind'] = sample_sub['ind']
    merged = merged.sort_values(['ind'])
    merged = merged.drop(['ind'],axis=1)
    merged.to_csv(output_path,index=False)


# In[16]:


def merge_submit(ld_pred_csv_path,bj_pred_csv_path,output_path,sample_sub_path,today):
    ld_pred = pd.read_csv(ld_pred_csv_path)
    bj_pred = pd.read_csv(bj_pred_csv_path)
    merged = ld_pred.append(bj_pred)
    sample_sub = pd.read_csv(sample_sub_path)
    if(len(merged)!=len(sample_sub)):
        print('The length(hours) of sample doesn\'t match.')
    sample_sub['ind'] = pd.Series(np.arange(len(sample_sub)))
    sample_sub = sample_sub.sort_values(['test_id'])
    merged = merged.sort_values(['test_id'])
    merged = merged.reset_index(drop=True)
    sample_sub = sample_sub.reset_index(drop=True)
    merged['ind'] = sample_sub['ind']
    merged = merged.sort_values(['ind'])
    merged = merged.drop(['ind'],axis=1)
    merged['submit_date'] = today
    merged = merged[['submit_date','test_id','PM2.5','PM10','O3']]
    merged.to_csv(output_path,index=False)


# In[28]:


'''
today = (datetime.now() + timedelta(hours=-8)).strftime('%m-%d')
merge_internal(ld_pred_csv_path='ld_inter.csv',bj_pred_csv_path='bj_inter.csv',
               output_path='output/valid_%s_07.csv'%(today),sample_sub_path='internal_val')
#merge_internal(ld_pred_csv_path='ld_inter.csv',bj_pred_csv_path='bj_inter.csv',
#               output_path='inter.csv',sample_sub_path='/nfs/Sif/runaway/20180301_20180331_2cities_sample_submission.csv')
'''


# In[27]:


'''
today = (datetime.now() + timedelta(hours=-8)).strftime('%m-%d')
merge_submit(ld_pred_csv_path='ld_submit.csv',bj_pred_csv_path='bj_submit.csv',
             output_path='output/test_%s_07.csv'%(today),sample_sub_path='/nfs/Sif/runaway/kdd_data/sample_submission.csv')
'''


# In[55]:


if __name__=='__main__':
    parser = argparser.ArgumentParser()
    parser.add_argument('--submit_target', type=str)
    parser.add_argument('--ld_input', type=str)
    parser.add_argument('--bj_input', type=str)
    parser.add_argument('--out_path', type=str)
    parser.add_argument('--sample_sub_path', type=str)
    args = parser.parse_args()
    if args.submit_target == 'internal':
        merge_internal( ld_pred_csv_path = args.ld_input,
                        bj_pred_csv_path = args.bj_input,
                        output_path      = args.out_path,
                        sample_sub_path  = args.sample_sub_path )
    elif args.submit_target == 'kdd_api':
        merge_submit(   ld_pred_csv_path = args.ld_input,
                        bj_pred_csv_path = args.bj_input,
                        output_path      = args.out_path,
                        sample_sub_path  = args.sample_sub_path )
    else:
        print('submit_target has WRONG argument.')


# In[ ]:




