
# coding: utf-8

# In[1]:


import london_tool
import beijing_tool
import pandas as pd
import numpy as np
from sklearn import ensemble
from sklearn import datasets
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from datetime import datetime, timedelta
from merge_csv import *
import os
import pickle
import sys


# In[2]:


cur_dir = sys.argv[1] + '/'
#cur_dir = '/nfs/Sif/runaway/mao/'


# In[3]:





# In[4]:


#_ = os.system('cp /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/bj_train_002.csv /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/bj_train.csv')
#_ = os.system('cp /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/ld_train_002.csv /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/ld_train.csv')


# In[5]:


#today = (datetime.now() + timedelta(hours=-8)).strftime('%Y-%m-%d')
#end_date = (datetime.now() + timedelta(hours=-8,days=-3)).strftime('%Y-%m-%d')
today = sys.argv[2]
end_date = (datetime.strptime(today, '%Y-%m-%d') + timedelta(days=-3)).strftime('%Y-%m-%d')

# In[6]:


X_val_bj,y_val_bj = beijing_tool.get_bj_daybyday_data(start='2018-03-31',end=end_date)
X_val_ld,y_val_ld =  london_tool.get_ld_daybyday_data(start='2018-03-31',end=end_date)


# In[7]:


X_today_ld,y_today_ld =  london_tool.get_ld_daybyday_data(start=today,end=today)
X_today_bj,y_today_bj = beijing_tool.get_bj_daybyday_data(start=today,end=today)


# In[8]:


def resh(X):
    return X.reshape((X.shape[0],X.shape[1]*X.shape[2]))
y_val_bj = y_val_bj[:,:-1,:]
y_val_ld = y_val_ld[:,:-2,:]
y_today_bj = y_today_bj[:,:-1,:]
y_today_ld = y_today_ld[:,:-2,:]


# In[9]:


bj_fea_n = int(X_val_bj.shape[2]/35)
ld_fea_n = int(X_val_ld.shape[2]/13)
print('Beijing has %d features,\nLondon has %d features.'%(bj_fea_n,ld_fea_n))


# In[10]:


with open(cur_dir + 'models/gbr_ld_interval_%s.pickle'%(today),'rb') as f:
    clf_train_ld_list = pickle.load(f)


# In[11]:


for i in range(13):
    ind1 = [i+13*j for j in range(ld_fea_n)]
    ind2 = [i+13*j for j in range(2)]
    clf_train_ld_list[i].set_params(n_jobs=1)
    pr = clf_train_ld_list[i].predict(resh(X_val_ld[:,:,ind1]))
    pr[pr<0] = 0.1
    y_val_ld[:,:,ind2] = pr.reshape((pr.shape[0],int(pr.shape[1]/2),2))


# In[12]:


l = len(clf_train_ld_list)
for i in range(l):
    del clf_train_ld_list[0]


# In[13]:


with open(cur_dir + 'models/gbr_bj_interval_%s.pickle'%(today),'rb') as f:
    clf_train_bj_list = pickle.load(f)


# In[14]:


for i in range(35):
    ind1 = [i+35*j for j in range(bj_fea_n)]
    ind2 = [i+35*j for j in range(3)]
    clf_train_bj_list[i].set_params(n_jobs=1)
    pr = clf_train_bj_list[i].predict(resh(X_val_bj[:,:,ind1]))
    pr[pr<0] = 0.1
    y_val_bj[:,:,ind2] = pr.reshape((pr.shape[0],int(pr.shape[1]/3),3))


# In[15]:


l = len(clf_train_bj_list)
for i in range(l):
    del clf_train_bj_list[0]


# In[16]:


with open(cur_dir + 'models/gbr_ld_submit_%s.pickle'%(today),'rb') as f:
    clf_retrain_ld_list = pickle.load(f)


# In[17]:


for i in range(13):
    ind1 = [i+13*j for j in range(ld_fea_n)]
    ind2 = [i+13*j for j in range(2)]
    clf_retrain_ld_list[i].set_params(n_jobs=1)
    pr = clf_retrain_ld_list[i].predict(resh(X_today_ld[:,:,ind1]))
    pr[pr<0] = 0.1
    y_today_ld[:,:,ind2] = pr.reshape((pr.shape[0],int(pr.shape[1]/2),2))


# In[18]:


l = len(clf_retrain_ld_list)
for i in range(l):
    del clf_retrain_ld_list[0]


# In[19]:


with open(cur_dir + 'models/gbr_bj_submit_%s.pickle'%(today),'rb') as f:
    clf_retrain_bj_list = pickle.load(f)


# In[20]:


for i in range(35):
    ind1 = [i+35*j for j in range(bj_fea_n)]
    ind2 = [i+35*j for j in range(3)]
    clf_retrain_bj_list[i].set_params(n_jobs=1)
    pr = clf_retrain_bj_list[i].predict(resh(X_today_bj[:,:,ind1]))
    pr[pr<0] = 0.1
    y_today_bj[:,:,ind2] = pr.reshape((pr.shape[0],int(pr.shape[1]/3),3))


# In[21]:


l = len(clf_retrain_bj_list)
for i in range(l):
    del clf_retrain_bj_list[0]


# In[22]:


y_today_ld = y_today_ld.reshape((y_today_ld.shape[1],y_today_ld.shape[2]))
y_today_bj = y_today_bj.reshape((y_today_bj.shape[1],y_today_bj.shape[2]))
_ = london_tool.ld_pred_to_internal_csv(y_pred=y_val_ld,
                                        output_csv_path=cur_dir + 'output/ld_inter.csv',
                                        end=end_date)
_ = beijing_tool.bj_pred_to_internal_csv(cur_dir,y_pred=y_val_bj,
                                         output_csv_path=cur_dir + 'output/bj_inter.csv',
                                         end=end_date)
_ = london_tool.ld_pred_to_submitable_csv(y_pred=y_today_ld,
                                          output_csv_path=cur_dir + 'output/ld_submit.csv')
_ = beijing_tool.bj_pred_to_submitable_csv(y_pred=y_today_bj,
                                           output_csv_path=cur_dir + 'output/bj_submit.csv')


# In[23]:


#today = (datetime.now() + timedelta(hours=-8)).strftime('%m-%d')
merge_internal(cur_path=cur_dir,ld_pred_csv_path = cur_dir + 'output/ld_inter.csv',
               bj_pred_csv_path = cur_dir + 'output/bj_inter.csv',
               output_path = cur_dir + '../../output/valid/valid_%s_07.csv'%(today[5:]),
               sample_sub_path='internal_val',end=end_date)
merge_submit(ld_pred_csv_path = cur_dir + 'output/ld_submit.csv',
             bj_pred_csv_path = cur_dir + 'output/bj_submit.csv',
             output_path = cur_dir + '../../output/test/test_%s_07.csv'%(today[5:]),
             sample_sub_path = cur_dir + 'sam_sub2.csv',today=today)




# In[ ]:




