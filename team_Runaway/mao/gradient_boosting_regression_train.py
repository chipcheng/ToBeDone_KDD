
# coding: utf-8

# In[1]:


import london_tool
import beijing_tool
import pandas as pd
import numpy as np
from sklearn.multioutput import MultiOutputRegressor
from sklearn import ensemble
from sklearn import datasets
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error
from datetime import datetime, timedelta
import os
import sys
import pickle


# In[2]:

cur_dir = sys.argv[1] + '/'
#cur_dir = '/nfs/Sif/runaway/mao/'


# In[3]:


best_data_id = '007'


# In[4]:


#_ = os.system('cp /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/bj_train_002.csv /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/bj_train.csv')
#_ = os.system('cp /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/ld_train_002.csv /nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/ld_train.csv')


# In[5]:


#today = (datetime.now() + timedelta(hours=-8)).strftime('%Y-%m-%d')
#end_date = (datetime.now() + timedelta(hours=-8,days=-3)).strftime('%Y-%m-%d')
today = sys.argv[2]
end_date = (datetime.strptime(today, '%Y-%m-%d') + timedelta(days=-3)).strftime('%Y-%m-%d')


# In[6]:


X_train_ld,y_train_ld =  london_tool.get_ld_daybyday_data(start='2017-01-02',end='2018-03-30')
X_train_bj,y_train_bj = beijing_tool.get_bj_daybyday_data(start='2017-01-02',end='2018-03-30')


# In[7]:


X_retrain_ld,y_retrain_ld =  london_tool.get_ld_daybyday_data(start='2017-01-02',end=end_date)
X_retrain_bj,y_retrain_bj = beijing_tool.get_bj_daybyday_data(start='2017-01-02',end=end_date)


# In[8]:


def resh(X):
    return X.reshape((X.shape[0],X.shape[1]*X.shape[2]))
y_train_bj = y_train_bj[:,:-1,:]
y_train_ld = y_train_ld[:,:-2,:]
y_retrain_bj = y_retrain_bj[:,:-1,:]
y_retrain_ld = y_retrain_ld[:,:-2,:]


# In[9]:


bj_fea_n = int(X_train_bj.shape[2]/35)
ld_fea_n = int(X_train_ld.shape[2]/13)
print('Beijing has %d features,\nLondon has %d features.'%(bj_fea_n,ld_fea_n))


# In[10]:


clf_train_ld_list = []
for i in range(13):
    clf = ensemble.GradientBoostingRegressor(n_estimators=150, max_depth=3,learning_rate=0.01,verbose=0)
    ind1 = [i+13*j for j in range(ld_fea_n)]
    ind2 = [i+13*j for j in range(2)]
    #print(ind1,ind2)
    print('training: London -- train -- city(%d/13)'%(i+1))
    clf = MultiOutputRegressor(clf,n_jobs=-1).fit(resh(X_train_ld[:,:,ind1]), resh(y_train_ld[:,:,ind2]))
    clf_train_ld_list.append(clf)


# In[11]:


clf_train_bj_list = []
for i in range(35):
    clf = ensemble.GradientBoostingRegressor(n_estimators=150, max_depth=3,learning_rate=0.01,verbose=0)
    ind1 = [i+35*j for j in range(bj_fea_n)]
    ind2 = [i+35*j for j in range(3)]
    #print(ind1,ind2)
    print('training: Beijing -- train -- city(%d/35)'%(i+1))
    clf = MultiOutputRegressor(clf,n_jobs=-1).fit(resh(X_train_bj[:,:,ind1]), resh(y_train_bj[:,:,ind2]))
    clf_train_bj_list.append(clf)


# In[12]:


clf_retrain_ld_list = []
for i in range(13):
    clf = ensemble.GradientBoostingRegressor(n_estimators=150, max_depth=3,learning_rate=0.01,verbose=0)
    ind1 = [i+13*j for j in range(ld_fea_n)]
    ind2 = [i+13*j for j in range(2)]
    #print(ind1,ind2)
    print('training: London -- retrain -- city(%d/13)'%(i+1))
    clf = MultiOutputRegressor(clf,n_jobs=-1).fit(resh(X_retrain_ld[:,:,ind1]), resh(y_retrain_ld[:,:,ind2]))
    clf_retrain_ld_list.append(clf)


# In[13]:


clf_retrain_bj_list = []
for i in range(35):
    clf = ensemble.GradientBoostingRegressor(n_estimators=150, max_depth=3,learning_rate=0.01,verbose=0)
    ind1 = [i+35*j for j in range(bj_fea_n)]
    ind2 = [i+35*j for j in range(3)]
    #print(ind1,ind2)
    print('training: Beijing -- retrain -- city(%d/35)'%(i+1))
    clf = MultiOutputRegressor(clf,n_jobs=-1).fit(resh(X_retrain_bj[:,:,ind1]), resh(y_retrain_bj[:,:,ind2]))
    clf_retrain_bj_list.append(clf)


# In[14]:


with open(cur_dir + 'models/gbr_bj_interval_%s.pickle'%(today),'wb') as f:
    pickle.dump(clf_train_bj_list, f)
with open(cur_dir + 'models/gbr_ld_interval_%s.pickle'%(today),'wb') as f:
    pickle.dump(clf_train_ld_list, f)
with open(cur_dir + 'models/gbr_bj_submit_%s.pickle'%(today),'wb') as f:
    pickle.dump(clf_retrain_bj_list, f)
with open(cur_dir + 'models/gbr_ld_submit_%s.pickle'%(today),'wb') as f:
    pickle.dump(clf_retrain_ld_list, f)


# In[ ]:




