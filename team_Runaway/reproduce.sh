#!/bin/bash

#Usage: ./reproduce.sh [date] Ex: ./reproduce.sh ./team_Runaway 05-01

for method in median mean
do
  python3 $2/amy/handle_history.py -aq $2/data -meta $2/data \
                                   -o $2/../output/test \
                                   -m $method --end-date 2018-$1 

  python3 $2/amy/handle_history.py -aq $2/data -meta $2/data \
                                   -o $2/../output/valid -v \
                                   -m $method --end-date 2018-$1 
done

bash $2/mao/gbr_train.sh $2/mao 2018-$1
bash $2/mao/gbr_test.sh  $2/mao 2018-$1

#ex: ./gbr_test.sh ~/ToBeDone_KDD/team_Runaway/mao 2018-05-01
