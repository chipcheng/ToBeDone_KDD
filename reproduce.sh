# $1-->05-01
echo 'Usage: ./reproduce $1(ex:05-01)'
echo 'Date : '$1
cur_dir=`pwd`

#---------------------Runaway part----------------------------
pip3 install -r ./team_Runaway/requirements.txt
bash ./team_Runaway/reproduce.sh $1 ./team_Runaway

cd $cur_dir

#---------------------TBD part----------------------------
pip3 install -r ./team_TBD/requirements.txt
cd ./team_TBD
bash reproduce.sh $1

cd $cur_dir

#---------------------CheeseBurger part----------------------------
pip3 install -r ./team_cheeseburger/requirements.txt
cd ./team_cheeseburger
bash run_rf.sh $1
bash run_lgb.sh $1

cd $cur_dir/ensemble
bash ensemble.sh $1

